﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TestDataService.Models;
using System.Configuration;

namespace TestDataService.Controllers
{
    public class ExcelController : ApiController
    {
        // GET api/excel/test.xls
        public Object Get(string name)
        {
            string folder = ConfigurationManager.AppSettings["ExcelFolder"];
            return ExcelReader.GetExcelData(folder.EndsWith("\\") ? folder + name : folder + "\\" + name);
        }
    }
}