﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TestDataService.Models;
using System.Configuration;

namespace TestDataService.Controllers
{
    public class TestStatusController : ApiController
    {
        // GET api/teststatus/SearchTest
        public bool Get(string name)
        {
            return TestState.getState(name);
        }
    }
}