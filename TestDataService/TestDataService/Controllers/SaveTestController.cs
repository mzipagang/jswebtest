﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace TestDataService.Controllers
{
    public class SaveTestController : ApiController
    {
        // POST api/savetest/filename
        public void Post(string name, [FromBody]string value)
        {
            string folder = ConfigurationManager.AppSettings["SaveTestFolder"];
            StreamWriter writer = File.CreateText(folder.EndsWith("\\") ? folder + name : folder + "\\" + name);
            writer.Write(value);
            writer.Flush();
            writer.Close();
        }

    }
}
