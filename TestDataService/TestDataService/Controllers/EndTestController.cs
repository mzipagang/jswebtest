﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TestDataService.Models;
using System.Configuration;

namespace TestDataService.Controllers
{
    public class EndTestController : ApiController
    {
        // GET api/endtest/SearchTest
        public void Get(string name)
        {
            TestState.endTest(name);
        }
    }
}