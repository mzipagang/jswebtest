﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestDataService.Models
{
    public class TestState
    {
        private static Dictionary<string, bool> testStates = new Dictionary<string, bool>();

        public static void startTest(string testName)
        {
            if (testStates.ContainsKey(testName))
            {
                testStates[testName] = true;
            }
            else
            {
                testStates.Add(testName, true);
            }
        }

        public static void endTest(string testName)
        {
            if (testStates.ContainsKey(testName))
            {
                testStates[testName] = false;
            }
        }

        public static bool getState(string testName)
        {
            if (testStates.ContainsKey(testName))
            {
                return testStates[testName];
            }

            return false;
        }
    }
}