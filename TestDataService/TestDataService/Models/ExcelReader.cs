﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Excel;
using System.IO;
using System.Data;

namespace TestDataService.Models
{
    public class ExcelReader
    {
        public static Object GetExcelData(string filePath)
        {
            Dictionary<string, List<string>> data = new Dictionary<string,List<string>>();
            int size = 0;

            try
            {
                FileStream stream = File.Open(filePath, FileMode.Open, FileAccess.Read);

                IExcelDataReader excelReader = null;
                if (filePath.EndsWith(".xls"))
                {
                    excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                }
                else if (filePath.EndsWith(".xlsx"))
                {
                    excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                }

                if (excelReader != null)
                {
                    excelReader.IsFirstRowAsColumnNames = true;

                    DataSet dataset = excelReader.AsDataSet();
                    DataTable table = dataset.Tables[0];
                    size = table.Rows.Count;
                    foreach (DataColumn column in table.Columns)
                    {
                        data.Add(column.ColumnName, new List<string>());
                    }

                    foreach(DataRow row in table.AsEnumerable())
                    {
                        foreach (var key in data.Keys)
                        {
                            data[key].Add(row[key].ToString());
                        }
                    }

                    excelReader.Close();
                }

                return new { size = size, table = data };
            }
            catch (Exception)
            {
                throw new Exception("Can't read excel file");
            }
        }
    }
}