define([
  'jquery',
  'config'
], function(
  	$,
    config
){
  var Tester = function(){
    var getType = {};
    var minDelay = 250;
    var currentTest = null;
    var functionArray = [];
    var childWinContents = null;
    var childWin = null;
    var timer = null;
    var currentRun = null;
    var testCount = 0;
    var running = false;
    var runCompleteFunc = null;
    var testCompleteFunc = null;

    var alertAssert = false;
    var overrideAlert = false;
    var oldAlertFunc = null;

    var launchChildW = function(url) {
      var childW = null;
      if (childW != null){
        childW.window.close();
      } 

      wopts  = config.testWindowOptions;
      childW = window.open(url, 'childW', wopts);

      if (childW == null) {
         alert("Failed to load test window");
      }
    
      return childW;
    }


    var newRun = function(callback){
      if(!!childWin && !!childWin.window){
        endRun();
      }

      running = true;

      callback({newTest:newTest, endTest:endTest, click:click, dblclick:dblclick, fillText:fillText, delay:delay, setDropdown:setDropdown, setCheckbox:setCheckbox, clickAnchor:clickAnchor,
        waitForPageRefresh:waitForPageRefresh, loadResource:loadResource, waitForElementToLoad:waitForElementToLoad, runSource:runSource, assertElementExists:assertElementExists,
         assertHtmlEquals:assertHtmlEquals, assertHtmlContains:assertHtmlContains, assertTextEquals:assertTextEquals, assertTextContains:assertTextContains, assertStyleEquals:assertStyleEquals,
          assertStyleContains:assertStyleContains, assertAddressBarEquals:assertAddressBarEquals, assertAddressBarContains:assertAddressBarContains, assertAlertDisplayed:assertAlertDisplayed});

      currentRun = {
                      result:true,
                      message:'Success',
                      startTime:Date.now(),
                      endTime:null,
                      timeElapsedSec:null
                    };

      return {start:start};
    }

    var newTest = function(name){
      functionArray.push({
        next: null,
        context: null,
        func: function(){
          currentTest = {
                      name:name,
                      result:true,
                      message:'Success',
                      startTime:Date.now(),
                      endTime:null,
                      timeElapsedSec:null
          };

          callNextFunc(this.context, this.next);  
        }
      });

      testCount++;
    }

    var endTest = function(){
      functionArray.push({
        next: null,
        context: null,
        func: function(){
          if(!!currentRun && !!currentTest && !currentTest.result){
            currentRun.result = currentTest.result;

            if(!!currentTest.message){
              currentRun.message = currentTest.message;
            }
          }
          if(!!currentTest){
            currentTest.endTime = Date.now();
            currentTest.timeElapsedSec = (currentTest.endTime - currentTest.startTime)/1000.0;
          
            if(!!testCompleteFunc){
              testCompleteFunc({currentTest:currentTest});
            }
          }

          callNextFunc(this.context, this.next);  
        }
      });
    }

    var endRun = function(){
      running = false;
      clearTimeout(timer);
      timer = null;
      if(!!childWin && !!childWin.window){
        childWin.window.close();
      }
      functionArray = [];
      testCount = 0;

      if(!!currentRun){
        currentRun.endTime = Date.now();
        currentRun.timeElapsedSec = (currentRun.endTime - currentRun.startTime)/1000.0;       
      }

      if(!!runCompleteFunc){
        runCompleteFunc({currentRun:currentRun});
      }
    }

    var findElement = function(param){
      if(!!param.iframeSelector){
        return $(childWinContents).find(param.iframeSelector).contents().find(param.selector);
      }
      else{
        return $(childWinContents).find(param.selector);
      }
    }

    var didNotFindElementFailAndStop = function(param){
      didNotFindElementFail(param);

      currentRun.message = currentTest.message;
      currentRun.result = currentTest.result;

      if(!!currentTest){
        currentTest.endTime = Date.now();
        currentTest.timeElapsedSec = (currentTest.endTime - currentTest.startTime)/1000.0;
        
        testCompleteFunc({currentTest:currentTest});
      }

      endRun();
    }

    var didNotFindElementFail = function(param){
      currentTest.result = false;
      currentTest.message = (!!param.errMsg ? param.errMsg + ". " : "") + "Did not find element selected by '" + param.selector + "'";
      if(!!param.iframeSelector){
        currentTest.message += " inside of iframe selected by '" + param.iframeSelector + "'";
      }
    }

    var foundElementFail = function(param){
      currentTest.result = false;
      currentTest.message = (!!param.errMsg ? param.errMsg + ". " : "") + "Found element selected by '" + param.selector + "'";
      if(!!param.iframeSelector){
        currentTest.message += " inside of iframe selected by '" + param.iframeSelector + "'";
      }
    }

    var runSource = function(param){
      functionArray.push({
        next: null,
        context: null,
        func: function(){
          try{
            if(!!param.iframeSelector){
              $(childWin.document).contents().find(param.iframeSelector)[0].contentWindow.eval(param.source);
            }
            else{
              childWin.eval(param.source);
            }
            callNextFunc(this.context, this.next);  
          }
          catch(e){
            currentRun.message = currentTest.message = (!!param.errMsg ? param.errMsg + ". " : "") + "Could not run source: '" + param.source + "'";
            if(!!param.iframeSelector){
              currentRun.message += " inside of iframe selected by '" + param.iframeSelector + "'";
              currentTest.message += " inside of iframe selected by '" + param.iframeSelector + "'";
            }
            currentRun.result = currentTest.result = false;
            endTest();
            endRun();
          }
        }
      });

      waitForPageRefresh(param);
    }

    var waitForPageRefresh = function(param){
       functionArray.push({
        next: null,
        context: null,
        func: function(){
          var self = this;
          if(!!param && !!param.iframeSelector){
            $($(childWinContents).find(param.iframeSelector)[0].contentWindow.document).ready(function(){
              setTimeout(function(){
                childWinContents = $(childWin.document).contents();
                callNextFunc(self.context, self.next);
              }, !!config.delay && config.delay >= minDelay ? config.delay : minDelay);
            });
          }
          else{
            $(childWin.document).ready(function(){
              setTimeout(function(){
                childWinContents = $(childWin.document).contents();
                callNextFunc(self.context, self.next);
              }, !!config.delay && config.delay >= minDelay ? config.delay : minDelay);
            });
          }
        }
      });
    }

    var waitForElementToLoad = function(param){
      functionArray.push({
        next: null,
        context: null,
        func: function(){
          var self = this;
          var tries = 1;
          if(isNaN(parseInt(param.retries))){
            param.retries = 5;
          }
          if(isNaN(parseInt(param.timeout))){
            param.timeout = config.delay;
          }

          if(findElement(param).length <= 0){
            var intervalometer = setInterval(function(){
              if(findElement(param).length > 0){
                clearInterval(intervalometer);
                callNextFunc(self.context, self.next);
              }
              else{
                try{
                  if(!!param && !!param.iframeSelector){
                    $($(childWinContents).find(param.iframeSelector)[0].contentWindow.document).ready(function(){
                      childWinContents = $(childWin.document).contents();                    
                    });
                  }
                  else{
                    $(childWin.document).ready(function(){
                      childWinContents = $(childWin.document).contents();
                    });
                  }
                }
                catch(e){
                  clearInterval(intervalometer);
                  endRun();
                }

                tries++;
                if(tries>=param.retries){
                  clearInterval(intervalometer);
                  callNextFunc(self.context, self.next);
                }
              }
            }, param.timeout);
          }
          else{
            callNextFunc(self.context, self.next);
          }
        }
      });
    }

    var clickAnchor = function(param){
      functionArray.push({
        next: null,
        context: null,
        func: function(){
          try{
            var input = findElement(param);
            if(input.length<1){
              didNotFindElementFailAndStop(param);
            }

            var href = input.attr("href");
            if(!!param.iframeSelector){
              $(childWin.document).contents().find(param.iframeSelector)[0].contentWindow.location = !!param.baseUrl ? (param.baseUrl + href) : href;
            }
            else{
              childWin.location = !!param.baseUrl ? (param.baseUrl + href) : href;
            }
            
            callNextFunc(this.context, this.next);
          }
          catch(e){
            didNotFindElementFailAndStop(param);
          }
        }
      });

      waitForPageRefresh(param);
    }

    var loadResource = function(param){
      functionArray.push({
        next: null,
        context: null,
        func: function(){
          if(!!param.iframeSelector){
            $(childWin.document).contents().find(param.iframeSelector)[0].contentWindow.location = param.url;
          }
          else{
            childWin.location = param.url;
          }
          callNextFunc(this.context, this.next);
        }
      });

      waitForPageRefresh(param);
    }

    var dblclick = function(param){
      functionArray.push({
        next: null,
        context: null,
        func: function(){
          try{
            var input = findElement(param);
            if(input.length<1){
              didNotFindElementFailAndStop(param);
            }
            input.dblclick();  
            callNextFunc(this.context, this.next);
          }
          catch(e){
            didNotFindElementFailAndStop(param);
          }
        }
      });

      waitForPageRefresh(param);
    }    

    var click = function(param){
      functionArray.push({
        next: null,
        context: null,
        func: function(){
          try{
            var input = findElement(param);
            if(input.length<1){
              didNotFindElementFailAndStop(param);
            }
            input.click();  
            callNextFunc(this.context, this.next);
          }
          catch(e){
            didNotFindElementFailAndStop(param);
          }
        }
      });

      waitForPageRefresh(param);
    }

    var fillText = function(param){
      functionArray.push({
        next: null,
        context: null,
        func: function(){
          try{
            var input = findElement(param);
            if(input.length<1){
              didNotFindElementFailAndStop(param);
            }
            if(!!param.nonInput){
              input.text(param.value);
            }
            else{
              input.val(param.value);
            }
            callNextFunc(this.context, this.next);
          }
          catch(e){
            didNotFindElementFailAndStop(param);
          }
        }
      });

      waitForPageRefresh(param);
    }

    var setCheckbox = function(param){
      functionArray.push({
        next: null,
        context: null,
        func: function(){
          try{
            var input = findElement(param);
            if(input.length<1){
              didNotFindElementFailAndStop(param);
            }
            input.prop('checked', param.isChecked);
            callNextFunc(this.context, this.next);
          }
          catch(e){
            didNotFindElementFailAndStop(param);
          }
        }
      });

      waitForPageRefresh(param);
    }

    var setDropdown = function(param){
      functionArray.push({
        next: null,
        context: null,
        func: function(){
          try{
            var input = findElement(param);
            if(input.length<1){
              didNotFindElementFailAndStop(param);
            }
            input.prop('selected', true);
            callNextFunc(this.context, this.next);
          }
          catch(e){
            didNotFindElementFailAndStop(param);
          }
        }
      });

      waitForPageRefresh(param);
    }

    var assertAddressBarEquals = function(param){
      functionArray.push({
        next: null,
        context: null,
        func: function(){
          var result;
          if(!!param.failIfTrue){
            result = param.url !== childWin.location.href;
          }
          else{
            result = param.url === childWin.location.href;
          }
          if(!result){
            if(!!param.failIfTrue){
              currentTest.message = (!!param.errMsg ? param.errMsg + ". " : "") + "Assert not equals failed. Expected: '" + param.url + "'. Actual: '" + childWin.location.href + "'";
            }
            else{
             currentTest.message = (!!param.errMsg ? param.errMsg + ". " : "") + "Assert equals failed. Expected: '" + param.url + "'. Actual: '" + childWin.location.href + "'"; 
            }
          }
          if(!!currentTest.result){
            currentTest.result = result;
          }

          callNextFunc(this.context, this.next);
        }
      });
    }

    var assertAddressBarContains = function(param){
      functionArray.push({
        next: null,
        context: null,
        func: function(){
          var result;
          if(!!param.failIfTrue){
            result = !(childWin.location.href.indexOf(param.url) >= 0);
          }
          else{
            result = childWin.location.href.indexOf(param.url) >= 0;
          }
          if(!result){
            if(!!param.failIfTrue){
              currentTest.message = (!!param.errMsg ? param.errMsg + ". " : "") + "Assert failed. '" + childWin.location.href + "' contains '" + param.url + "'";
            }
            else{
              currentTest.message = (!!param.errMsg ? param.errMsg + ". " : "") + "Assert failed. '" + childWin.location.href + "' doesn't contain '" + param.url + "'";
            }
          }
          if(!!currentTest.result){
            currentTest.result = result;
          }

          callNextFunc(this.context, this.next);
        }
      });
    }

    var assertAlertDisplayed = function(param){
      overrideAlert = true;
      functionArray.push({
        next: null,
        context: null,
        func: function(){
          var result;
          if(!!param.failIfTrue){
            result = !alertAssert;
          }
          else{
            result = alertAssert;
          }
          if(!result){
            if(!!param.failIfTrue){
              currentTest.message = ((!!param && !!param.errMsg) ? param.errMsg + ". " : "") + "Assert failed. Alert was displayed.'";
            }
            else{
              currentTest.message = ((!!param && !!param.errMsg) ? param.errMsg + ". " : "") + "Assert failed. Alert was not displayed.'"; 
            }
          }
          if(!!currentTest.result){
            currentTest.result = result;
          }
          // reset
          alertAssert = false;

          callNextFunc(this.context, this.next);
        }
      });
    }

    var assertElementExists = function(param){
      functionArray.push({
        next: null,
        context: null,
        func: function(){
          var elem = findElement(param);
          if(!!param.failIfTrue){
            if(elem.length > 0){
              foundElementFail(param);
            }  
          }
          else{
           if(elem.length < 1){
              didNotFindElementFail(param);
            } 
          }

          callNextFunc(this.context, this.next);
        }
      });
    }

    var assertStyleEquals = function(param){
      functionArray.push({
        next: null,
        context: null,
        func: function(){
          var elem = findElement(param);
          if(elem.length > 0){
            var result;
            if(!!param.failIfTrue){
              result = elem.css(param.attribute) !== param.value;
            }
            else{
              result = elem.css(param.attribute) === param.value;
            }
            if(!result){
              if(!!param.failIfTrue){
                currentTest.message = (!!param.errMsg ? param.errMsg + ". " : "") + "Assert not equals failed. Expected: '" + param.value + "'. Actual: '" + elem.css(param.attribute) + "'";
              }
              else{
                currentTest.message = (!!param.errMsg ? param.errMsg + ". " : "") + "Assert equals failed. Expected: '" + param.value + "'. Actual: '" + elem.css(param.attribute) + "'";
              }
            }
            if(!!currentTest.result){
              currentTest.result = result;
            }
          }
          else{
            didNotFindElementFail(param);
          }

          callNextFunc(this.context, this.next);
        }
      });
    }

    var assertStyleContains = function(param){
      functionArray.push({
        next: null,
        context: null,
        func: function(){
          var elem = findElement(param);
          if(elem.length > 0){
            var result;
            if(!!param.failIfTrue){
              result = !(elem.css(param.attribute).indexOf(param.value) >= 0);
            }
            else{
              result = elem.css(param.attribute).indexOf(param.value) >= 0;
            }
            if(!result){
              if(!!param.failIfTrue){
                currentTest.message = (!!param.errMsg ? param.errMsg + ". " : "") + "Assert failed. '" + elem.css(param.attribute) + "' contains '" + param.value + "'";
              }
              else{
                currentTest.message = (!!param.errMsg ? param.errMsg + ". " : "") + "Assert failed. '" + elem.css(param.attribute) + "' doesn't contain '" + param.value + "'";
              }
            }
            if(!!currentTest.result){
              currentTest.result = result;
            }
          }
          else{
            didNotFindElementFail(param);
          }
          
          callNextFunc(this.context, this.next);
        }
      });
    }

    var assertHtmlEquals = function(param){
      functionArray.push({
        next: null,
        context: null,
        func: function(){
          var elem = findElement(param);
          if(elem.length > 0){
            var result;
            if(!!param.failIfTrue){
              result = elem.html() !== param.value;
            }
            else{
              result = elem.html() === param.value;
            }
            if(!result){
              if(!!param.failIfTrue){
                currentTest.message = (!!param.errMsg ? param.errMsg + ". " : "") + "Assert not equals failed. Expected: '" + param.value + "'. Actual: '" + elem.html() + "'";
              }
              else{
                currentTest.message = (!!param.errMsg ? param.errMsg + ". " : "") + "Assert equals failed. Expected: '" + param.value + "'. Actual: '" + elem.html() + "'";
              }
            }
            if(!!currentTest.result){
              currentTest.result = result;
            }
          }
          else{
            didNotFindElementFail(param);
          }

          callNextFunc(this.context, this.next);
        }
      });
    }

    var assertHtmlContains = function(param){
      functionArray.push({
        next: null,
        context: null,
        func: function(){
          var elem = findElement(param);
          if(elem.length > 0){
            var result;
            if(!!param.failIfTrue){
              result = !(elem.html().indexOf(param.value) >= 0);
            }
            else{
              result = elem.html().indexOf(param.value) >= 0;
            }
            if(!result){
              if(!!param.failIfTrue){
                currentTest.message = (!!param.errMsg ? param.errMsg + ". " : "") + "Assert failed. '" + elem.html() + "' contains '" + param.value + "'";
              }
              else{
                currentTest.message = (!!param.errMsg ? param.errMsg + ". " : "") + "Assert failed. '" + elem.html() + "' doesn't contain '" + param.value + "'";
              }
            }
            if(!!currentTest.result){
              currentTest.result = result;
            }
          }
          else{
            didNotFindElementFail(param);
          }
          
          callNextFunc(this.context, this.next);
        }
      });
    }

    var assertTextEquals = function(param){
      functionArray.push({
        next: null,
        context: null,
        func: function(){
          var elem = findElement(param);
          if(elem.length > 0){
            var result;
            if(!!param.failIfTrue){
              result = elem.text() !== param.value;
            }
            else{
              result = elem.text() === param.value;
            }
            if(!result){
              if(!!param.failIfTrue){
                currentTest.message = (!!param.errMsg ? param.errMsg + ". " : "") + "Assert not equals failed. Expected: '" + param.value + "'. Actual: '" + elem.text() + "'";
              }
              else{
                currentTest.message = (!!param.errMsg ? param.errMsg + ". " : "") + "Assert equals failed. Expected: '" + param.value + "'. Actual: '" + elem.text() + "'";
              }
            }
            if(!!currentTest.result){
              currentTest.result = result;
            }
          }
          else{
            didNotFindElementFail(param);
          }
          
          callNextFunc(this.context, this.next);
        }
      });
    }

    var assertTextContains = function(param){
      functionArray.push({
        next: null,
        context: null,
        func: function(){
          var elem = findElement(param);
          if(elem.length > 0){
            var result;
            if(!!param.failIfTrue){
              result = !(elem.text().indexOf(param.value) >= 0);
            }
            else{
              result = elem.text().indexOf(param.value) >= 0;
            }            
            if(!result){
              if(!!param.failIfTrue){
                currentTest.message = (!!param.errMsg ? param.errMsg + ". " : "") + "Assert failed. '" + elem.text() + "' contains '" + param.value + "'";
              }
              else{
                currentTest.message = (!!param.errMsg ? param.errMsg + ". " : "") + "Assert failed. '" + elem.text() + "' doesn't contain '" + param.value + "'";
              }
            }
            if(!!currentTest.result){
              currentTest.result = result;
            }
          }
          else{
            didNotFindElementFail(param);
          }
          
          callNextFunc(this.context, this.next);
        }
      });
    }

    var delay = function(waitTimeMilliseconds){
      functionArray.push({
        next: null,
        context: null,
        func: function(){
          var self = this;
          timer = setTimeout(function(){
            callNextFunc(self.context, self.next);
          }, waitTimeMilliseconds);
        }
      }); 
    }

    var callNextFunc = function(context, func){
      if(!running){
        return;
      }

      timer = setTimeout(function(){
        if(!!func){
          func.call(context);
        }
        else{
          endRun();
        }
      }, !!config.delay && config.delay >= minDelay ? config.delay : minDelay);
      
    }

    var start = function(startUrl, preRunCallback, postRunCallback, postTestCallback){

      childWin = launchChildW(startUrl);

      $(childWin.document).ready(function() {
          
          if(!!oldAlertFunc){
            childWin.window.alert = oldAlertFunc;
            oldAlertFunc = null;
          }

          // hook up alert assert
          if(overrideAlert){
            oldAlertFunc = childWin.window.alert;
            childWin.window.alert = function() {
              alertAssert = true;
            };
          }

          overrideAlert = false;

          childWinContents = $(childWin.document).contents();

          // chain callbacks
          for(var ii=0;ii<functionArray.length;ii++){
            if(ii+1 < functionArray.length){
              functionArray[ii].next = functionArray[ii+1].func;
              functionArray[ii].context = functionArray[ii+1];
            }
          }

          // setup callbacks
          if(!!preRunCallback && getType.toString.call(preRunCallback) === '[object Function]'){
            preRunCallback({testCount:testCount});
          }
            
          if(!!postRunCallback && getType.toString.call(postRunCallback) === '[object Function]'){
            runCompleteFunc = postRunCallback;
          }

          if(!!postTestCallback && getType.toString.call(postTestCallback) === '[object Function]'){
            testCompleteFunc = postTestCallback;
          }

          // start run
          functionArray[0].func();  
      }); 
    }
    
    return {newRun:newRun, endRun:endRun};  
  }();
  
  return Tester;
});