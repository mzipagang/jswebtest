define([
], function(){ 
	var Config = {
		httpBaseUrl:"http://www.anymeeting-staging.com/",
		httpsBaseUrl:"https://www.anymeeting-staging.com/",		

		testWindowOptions:'width=1440,height=960,resizable=1,menubar=0,toolbar=0,alwaysRaised=1,scrollbars=1',
		delay:1000,

		// test data
		getTestDataBaseUrl:'http://54.225.237.249/api/excel',
		saveTestResultsBaseUrl:'http://54.225.237.249/api/savetest',
		startTestBaseUrl:'http://54.225.237.249/api/starttest',
		endTestBaseUrl:'http://54.225.237.249/api/endtest',
		testStatusBaseUrl:'http://54.225.237.249/api/teststatus',


		tests: [{testName:'LoginTest', url: 'login.html'},
                 {testName:'CreateMeetingRegistrationTest', url: 'createmeetingregistration.html'},
                 {testName:'CompleteMeetingRegistrationTest', url: 'completemeetingregistration.html'},
                 {testName:'CreateMeetingSurveyTest', url: 'createmeetingsurvey.html'},
                 {testName:'CompleteMeetingSurveyTest', url: 'completemeetingsurvey.html'},
                 {testName:'ScheduleMeetingTest', url: 'schedulemeeting.html'},
                 {testName:'StartMeetingTest', url: 'startmeeting.html'},
                 {testName:'LogoutTest', url: 'logout.html'},
                 {testName:'SignupTest', url: 'signup.html'},
                 {testName:'UpgradePathTest', url: 'upgradepath.html'},
                 {testName:'DowngradeTest', url: 'downgrade.html'},
                 {testName:'PublicProfileTest', url: 'publicprofile.html'},
                 {testName:'DeactivateTest', url: 'deactivate.html'}]

        // RUN ALONE FOR CLEANUP (NEEDS TO BE LOGGED IN)
        //tests:  [{testName:'DeactivateTest', url: 'deactivate.html'}]
	};

	return Config;
});