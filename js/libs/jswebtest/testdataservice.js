define([
  'jquery',
  'config'
], function(
  	$,
    config
){
  var TestDataService = function(){
    
    var getTestData = function(filename, callback){
      var rootUrl = '';
      if(config.getTestDataBaseUrl.charAt(config.getTestDataBaseUrl.length - 1) === '/'){
        rootUrl = config.getTestDataBaseUrl.substring(0, config.getTestDataBaseUrl.length - 1);
      }
      else{
        rootUrl = config.getTestDataBaseUrl;
      }

      $.ajax({
        url:  rootUrl + '/' + filename + '/',
        type: 'GET',
        dataType: 'jsonp',
        success: function(data){
          var getType = {};
          if(!!callback && getType.toString.call(callback) === '[object Function]'){
            callback(data);
          }
        }
      });
    }

    var startTest = function(testName){
      var rootUrl = '';
      if(config.startTestBaseUrl.charAt(config.startTestBaseUrl.length - 1) === '/'){
        rootUrl = config.startTestBaseUrl.substring(0, config.startTestBaseUrl.length - 1);
      }
      else{
        rootUrl = config.startTestBaseUrl;
      }

      $.ajax({
        url:  rootUrl + '/' + testName + '/',
        type: 'GET',
        dataType: 'jsonp',
        success: function(){
          
        }
      });
    }

    var endTest = function(testName){
      var rootUrl = '';
      if(config.endTestBaseUrl.charAt(config.endTestBaseUrl.length - 1) === '/'){
        rootUrl = config.endTestBaseUrl.substring(0, config.endTestBaseUrl.length - 1);
      }
      else{
        rootUrl = config.endTestBaseUrl;
      }

      $.ajax({
        url:  rootUrl + '/' + testName + '/',
        type: 'GET',
        dataType: 'jsonp',
        success: function(){
          
        }
      });
    }

    var getTestStatus = function(testName, callback){
      var rootUrl = '';
      if(config.testStatusBaseUrl.charAt(config.testStatusBaseUrl.length - 1) === '/'){
        rootUrl = config.testStatusBaseUrl.substring(0, config.testStatusBaseUrl.length - 1);
      }
      else{
        rootUrl = config.testStatusBaseUrl;
      }

      $.ajax({
        url:  rootUrl + '/' + testName + '/',
        type: 'GET',
        dataType: 'jsonp',
        success: function(status){
          var getType = {};
          if(!!callback && getType.toString.call(callback) === '[object Function]'){
            callback(status);
          }
        }
      });
    }

    var saveTestResult = function(filename, testResults, callback){
      var rootUrl = '';
      if(config.saveTestResultsBaseUrl.charAt(config.saveTestResultsBaseUrl.length - 1) === '/'){
        rootUrl = config.saveTestResultsBaseUrl.substring(0, config.saveTestResultsBaseUrl.length - 1);
      }
      else{
        rootUrl = config.saveTestResultsBaseUrl;
      }

      $.ajax({
        url:  rootUrl + '/' + filename + '/',
        type: 'POST',
        data: "=" + testResults,
        success: function(){
          var getType = {};
          if(!!callback && getType.toString.call(callback) === '[object Function]'){
            callback();
          }
        }
      });
    }

    var charList="abcdefghijklmnopqrstuvwxyz1234567890";
    var generateAlphanumericString = function(length){
      if(isNaN(parseInt(length))){
        length = 6;
      }
      var randomString ='';
      for (ii=0;ii<length;ii++){
        randomString += charList.charAt(Math.floor(Math.random() * charList.length));
      }
      return randomString;
    }

    var letterList ="abcdefghijklmnopqrstuvwxyz";
    var generateAlphabeticString = function(length){
      if(isNaN(parseInt(length))){
        length = 6;
      }
      var randomString ='';
      for (ii=0;ii<length;ii++){
        randomString += letterList.charAt(Math.floor(Math.random() * letterList.length));
      }
      return randomString;
    }

    var numberList ="1234567890";
    var generateNumericString = function(length){
      if(isNaN(parseInt(length))){
        length = 6;
      }
      var randomString ='';
      for (ii=0;ii<length;ii++){
        randomString += numberList.charAt(Math.floor(Math.random() * numberList.length));
      }
      return randomString;
    }

    return {getTestData:getTestData, saveTestResult:saveTestResult, generateNumericString:generateNumericString, generateAlphabeticString:generateAlphabeticString, 
      generateAlphanumericString:generateAlphanumericString, startTest:startTest, endTest:endTest, getTestStatus:getTestStatus};
  }();
  
  return TestDataService;
});