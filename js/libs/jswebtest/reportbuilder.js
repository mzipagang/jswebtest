define([
  'jquery',
  'config',
  'testdataservice'
], function(
  	$,
    config,
    testdataservice
){
  var ReportBuilder = function(){
    var enabled = false;
    var testRunCount = 0;
    var containerSelector = null;
    var failedUrlCount = 0;
    var successfulUrlCount = 0;
    var totalCrawlTime = 0;

    var testReport = null;
    var crawlReport = null;

    var initWebTestReport = function(containerSelector, testCount){
      testRunCount = 0;
      this.containerSelector = containerSelector;
      $(containerSelector).html(
          "<div id='runCountHolder'>" +
            "Tests Run: <span id='runCount'>0</span>/<span id='testCount'>" + testCount + "</span>" +
          "</div>" +
          "<div id='testResultsHolder'>" +
          "</div>"
        );

      enabled = true;
    }

    var initLinkCrawlReport = function(containerSelector){
      testRunCount = 0;
      successfulUrlCount = 0;
      failedUrlCount = 0;
      this.containerSelector = containerSelector;
      $(containerSelector).html(
          "<div id='runCountHolder'>" +
            "Links Crawled: <span id='runCount'>0</span> (Avg. request time: <span id='timeElapsedSec'></span>)" +
          "</div><br/>" +
          "Failed URLs <span id='failedUrlCount'></span><br/><div id='failedUrlsHolder'>" +
          "</div><br/><br/>" +
          "Successful URLs <span id='successfulUrlCount'></span><br/><div id='successfulUrlsHolder'>" +
          "</div>"
        );

      enabled = true;
    }

    var renderTest = function(test){
      if(enabled){
        testRunCount++;
        $(this.containerSelector + '>#runCountHolder>#runCount').text(testRunCount);
        var temp = $(this.containerSelector + '>#testResultsHolder').html();
        $(this.containerSelector + '>#testResultsHolder').html(temp + 
          "<div id='testResult' " + (test.result ? "class='passed'" : "class='failed'") + ">" +
          test.name + ":  " + (test.result ? "PASSED" : "FAILED") + " (" + test.message + ") - " + test.timeElapsedSec + "s" +
          "</div>");
      }
    }

    var renderRun = function(run){
      if(enabled){
        var temp = $(this.containerSelector + '>#testResultsHolder').html();
        $(this.containerSelector + '>#testResultsHolder').html(temp + 
          "<div id='runResult' " + (run.result ? "class='passed'" : "class='failed'") + ">" +
          "RUN RESULT:  " + (run.result ? "PASSED" : "FAILED") + " (" + run.message + ") - " + run.timeElapsedSec + "s" +
          "</div>");
      }
    }

    var renderCrawl = function(url, result, timeElapsedSec){
      if(enabled){
        testRunCount++;
        totalCrawlTime += timeElapsedSec;
        $(this.containerSelector + '>#runCountHolder>#runCount').text(testRunCount);
        $(this.containerSelector + '>#runCountHolder>#timeElapsedSec').text((totalCrawlTime/testRunCount))
        if(!!result){
          successfulUrlCount++;
          $(this.containerSelector + '>#successfulUrlCount').html("(" + successfulUrlCount + ")");
          var temp = $(this.containerSelector + '>#successfulUrlsHolder').html();
          $(this.containerSelector + '>#successfulUrlsHolder').html(temp + 
            "<div id='testResult'>" + url +
            "</div>");
        }
        else{
          failedUrlCount++;
          $(this.containerSelector + '>#failedUrlCount').html("(" + failedUrlCount + ")");
          var temp = $(this.containerSelector + '>#failedUrlsHolder').html();
          $(this.containerSelector + '>#failedUrlsHolder').html(temp + 
            "<div id='testResult'>" + url +
            "</div>"); 
        }
      }
    }


    return {initWebTestReport:initWebTestReport, initLinkCrawlReport:initLinkCrawlReport, renderRun:renderRun, renderTest:renderTest, renderCrawl:renderCrawl};  
  }();
  
  return ReportBuilder;
});