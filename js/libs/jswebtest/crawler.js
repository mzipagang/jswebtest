define([
  'jquery',
  'config'
], function(
  	$,
    config
){
  var Crawler = function(){
    var minDelay = 250;
    var pendingUrls = {};
    var successfulUrls = {};
    var failedUrls = {};
    var skipUrls = {};
    var workStack = [];
    var currentUrl = null;

    var childWinContents = null;
    var childWin = null;
    var rootUrl = null;

    var getType = {};
    var testCompleteFunc = null;
    var resourceResultCallback = null;

    var startTime = null;
    var endTime = null;
    var timeElapsedSec = null;

    var launchChildW = function(url) {
      var childW = null;
      if (childW != null){
        childW.window.close();
      } 

      wopts  = config.testWindowOptions;
      childW = window.open(url, 'childW', wopts);

      if (childW == null) {
         alert("Failed to load test window");
      }
    
      return childW;
    }

    var stop = function(){
      // reset everything
      pendingUrls = {};
      successfulUrls = {};
      failedUrls = {};
      workStack = [];
      
      if(!!childWin && !!childWin.window){
        childWin.window.close();
      }

      if(!!testCompleteFunc){
        testCompleteFunc();
      }
    }

    var loadResource = function(url){
      childWin.location = url;
      
      setTimeout(function(){
        try{
          startTime = Date.now();

          $(childWin.document).ready(function() {
            setTimeout(function(){
              try{
                childWinContents = $(childWin.document).contents();
                currentUrl = childWin.location.href;
                delete pendingUrls[url];

                startTime = Date.now();

                urlExists(url, function(status){
                  if(status >= 400){
                    // this is a 404, just go to the next url
                    failedUrls[url] = true;
                    delete pendingUrls[url];

                    endTime = Date.now();
                    timeElapsedSec = (endTime - startTime)/1000.0;

                    if(!!resourceResultCallback){
                      resourceResultCallback(url, false, timeElapsedSec);
                    }
                    
                    loadNextResource();
                  }
                  else{
                    // not a 404, mark successful and keep scraping
                    successfulUrls[url] = true;
                    delete pendingUrls[url];

                    endTime = Date.now();
                    timeElapsedSec = (endTime - startTime)/1000.0;

                    if(!!resourceResultCallback){
                      resourceResultCallback(url, true, timeElapsedSec);
                    }
                    
                    scrapeUrls();
                  }
                });
              }
              catch(e){
                failedUrls[url] = true;
                delete pendingUrls[url];

                endTime = Date.now();
                timeElapsedSec = (endTime - startTime)/1000.0;

                if(!!resourceResultCallback){
                  resourceResultCallback(url, false, timeElapsedSec);
                }

                console.log(url + ' - ' + e);
                loadNextResource();
              }
            }, !!config.delay && config.delay >= minDelay ? config.delay : minDelay);
          });
        }
        catch(e){
          failedUrls[url] = true;
          delete pendingUrls[url];

          endTime = Date.now();
          timeElapsedSec = (endTime - startTime)/1000.0;

          if(!!resourceResultCallback){
            resourceResultCallback(url, false, timeElapsedSec);
          }

          console.log(url + ' - ' + e);
          loadNextResource();
        }
      },  !!config.delay && config.delay >= minDelay ? config.delay : minDelay);
    }

    var loadNextResource = function(){
      if(workStack.length > 0){
        loadResource(workStack.pop());
      }
      else{
        stop();
      }
    }


    var scrapeUrls = function(){
      var set = $(childWinContents).find("a");
      $.each(set, function(index, value){
        var url = $(value).attr("href");
        if(!!url){
          var fullUrl = null;
          if(url.indexOf("http") === 0 || url.indexOf("www") === 0){
            fullUrl = url;
          }
          else{
            if(url.indexOf('/') === 0){
              fullUrl = rootUrl + url;
            }
            else{
              fullUrl = rootUrl + '/' + url;
            }
          }

          if(fullUrl.indexOf(rootUrl) === -1){
            // outside of domain, can't access (XSS)
            skipUrls[fullUrl] = true;
          }
          if(!skipUrls[url] && !successfulUrls[fullUrl] && !failedUrls[fullUrl] && !pendingUrls[fullUrl]){
            pendingUrls[fullUrl] = true;
            workStack.push(fullUrl);
          }
        }
      });

      loadNextResource();
    }


    var urlExists = function(url, cb){
      $.ajax({
          url:      url,
          type:     'GET',
          complete:  function(xhr){
            if(typeof cb === 'function')
             cb.apply(this, [xhr.status]);
          }
      });
    }


    var start = function(baseUrl, startUrl, avoidUrls, preTestCallback, resourceReportCallback, postTestCallback){
      if(!baseUrl){
        throw "Must specify baseUrl";
        return;
      }
      if(!startUrl){
        throw "Must specify startUrl";
        return;
      }

      if(!!preTestCallback && getType.toString.call(preTestCallback) === '[object Function]'){
        preTestCallback();
      }      

      if(!!resourceReportCallback && getType.toString.call(resourceReportCallback) === '[object Function]'){
        resourceResultCallback = resourceReportCallback;
      }
      
      if(!!postTestCallback && getType.toString.call(postTestCallback) === '[object Function]'){
        testCompleteFunc = postTestCallback;
      }
      
      if(baseUrl.charAt(baseUrl.length - 1) === '/'){
        rootUrl = baseUrl.substring(0, baseUrl.length - 1);
      }
      else{
        rootUrl = baseUrl;
      }
      
      if(!!avoidUrls && Array.isArray(avoidUrls)){
        $.each(avoidUrls, function(index, value){
          skipUrls[value] = true;
        });
      }

      if(!!childWin && !!childWin.window){
        stop();
      }

      try{
        childWin = launchChildW(startUrl);
        $(childWin).load(function() {
            setTimeout(function(){
              childWinContents = $(childWin.document).contents();
              scrapeUrls();
            }, !!config.delay && config.delay >= minDelay ? config.delay : minDelay);
        });
      }
      catch(e){
        stop();
      }
    }
    
    return {start:start, stop:stop};  
  }();
  
  return Crawler;
});