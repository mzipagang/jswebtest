define([
  'tester',
  'testdataservice',
  'config',
  'reportbuilder'
], function(
    tester,
    testdataservice,
    config,
    reportbuilder
){

    var baseUrl = config.httpsBaseUrl;

    var homeUrl = baseUrl + "AccountManager/AnyMeeting.aspx";
    
    var reportOptions = {reportContainerSelector:"#test"}

    var testRun;
    

    $(function(){

        testRun = tester.newRun(function(utils){

            var ii=0;

            testdataservice.getTestData('schedulemeetingdata.xls', function(schedulemeetingdata){

                testdataservice.startTest('ScheduleMeetingTest');
                
                // SCHEDULE MEETING TESTS
                for(ii=0;ii<schedulemeetingdata.size;ii++){

                    utils.newTest("ScheduleMeetingTest  " + ((!!schedulemeetingdata.table.Description[ii]) ? ("(" + schedulemeetingdata.table.Description[ii] + ")") : ""));

                    if(ii>0){
                        utils.loadResource({url:homeUrl});    
                    }

                    utils.waitForElementToLoad({selector:"#ctl00_cphPageContent_btnScheduleMeeting", retries:30, timeout:1000});
                    utils.click({selector:"#ctl00_cphPageContent_btnScheduleMeeting"});

                    utils.waitForElementToLoad({selector:"#ctl00_cphPageContent_myWizard_txtMeetingTitle", retries:30, timeout:1000}); 
                    // fill title
                    utils.fillText({selector:"#ctl00_cphPageContent_myWizard_txtMeetingTitle", value:schedulemeetingdata.table.Title[ii]});
                    
                    // fill date
                    utils.fillText({selector:"#ctl00_cphPageContent_myWizard_MeetingScheduler1_txtMeetingStartDate", value:schedulemeetingdata.table.Date[ii]});

                    if(!!schedulemeetingdata.table.Time[ii]){
                        // fill time
                        utils.setDropdown({selector:'#ctl00_cphPageContent_myWizard_MeetingScheduler1_ddlTime>option[value="' + schedulemeetingdata.table.Time[ii] + '"]'});
                    }

                    if(!!schedulemeetingdata.table.AmPm[ii]){
                        // fill am/pm
                        utils.setDropdown({selector:'#ctl00_cphPageContent_myWizard_MeetingScheduler1_ddlAMPM>option[value="' + schedulemeetingdata.table.AmPm[ii] + '"]'});
                    }

                    if(!!schedulemeetingdata.table.Duration[ii]){
                        // fill duration
                        utils.setDropdown({selector:'#ctl00_cphPageContent_myWizard_MeetingScheduler1_ddlDuration>option[value="' + schedulemeetingdata.table.Duration[ii] + '"]'});
                    }

                    if(!!schedulemeetingdata.table.Timezone[ii]){
                        // fill timezone
                        utils.setDropdown({selector:'#ctl00_cphPageContent_myWizard_MeetingScheduler1_ddlTimeZone_ddlTimeZone>option[value="' + schedulemeetingdata.table.Timezone[ii] + '"]'});
                    }

                    if(schedulemeetingdata.table.Recurring[ii] === 'True'){
                        // set Recurring
                        utils.click({selector:'#ctl00_cphPageContent_myWizard_cntrlRecurrence_chkRecurs'});
                        utils.delay(2000);

                        if(!!schedulemeetingdata.table.Frequency[ii]){
                           // fill frequency
                            utils.setDropdown({selector:'#ctl00_cphPageContent_myWizard_cntrlRecurrence_ddlRecurrenceType>option[value="' + schedulemeetingdata.table.Frequency[ii] + '"]'});

                            if(schedulemeetingdata.table.Frequency[ii] === 'Daily'){
                                if(schedulemeetingdata.table.DailyFrequency[ii] === 'EveryWeekday'){
                                    utils.click({selector:"#ctl00_cphPageContent_myWizard_cntrlRecurrence_rdioEveryWeekday[value='rdioEveryWeekday']"});
                                    utils.delay(1000);
                                }
                                else if(schedulemeetingdata.table.DailyFrequency[ii] === 'NDays'){
                                    utils.click({selector:"#ctl00_cphPageContent_myWizard_cntrlRecurrence_rdioNDays[value='rdioNDays']"});
                                    utils.delay(1000);
                                    
                                    // fill n days
                                    utils.fillText({selector:"#ctl00_cphPageContent_myWizard_cntrlRecurrence_txtNDays", value:schedulemeetingdata.table.Ndays[ii]});
                                }
                            }
                            else if(schedulemeetingdata.table.Frequency[ii] === 'Weekly'){
                                // fill n weeks
                                utils.fillText({selector:"#ctl00_cphPageContent_myWizard_cntrlRecurrence_txtNWeeks", value:schedulemeetingdata.table.Nweeks[ii]});

                                if(schedulemeetingdata.table.Weekday[ii].indexOf('Sunday') >= 0){
                                    utils.click({selector:"#ctl00_cphPageContent_myWizard_cntrlRecurrence_chkSunday"});
                                }
                                if(schedulemeetingdata.table.Weekday[ii].indexOf('Monday') >= 0){
                                    utils.click({selector:"#ctl00_cphPageContent_myWizard_cntrlRecurrence_chkMonday"});
                                }
                                if(schedulemeetingdata.table.Weekday[ii].indexOf('Tuesday') >= 0){
                                    utils.click({selector:"#ctl00_cphPageContent_myWizard_cntrlRecurrence_chkTuesday"});
                                }
                                if(schedulemeetingdata.table.Weekday[ii].indexOf('Wednesday') >= 0){
                                    utils.click({selector:"#ctl00_cphPageContent_myWizard_cntrlRecurrence_chkWednesday"});
                                }
                                if(schedulemeetingdata.table.Weekday[ii].indexOf('Thursday') >= 0){
                                    utils.click({selector:"#ctl00_cphPageContent_myWizard_cntrlRecurrence_chkThursday"});
                                }
                                if(schedulemeetingdata.table.Weekday[ii].indexOf('Friday') >= 0){
                                    utils.click({selector:"#ctl00_cphPageContent_myWizard_cntrlRecurrence_chkFriday"});
                                }
                                if(schedulemeetingdata.table.Weekday[ii].indexOf('Saturday') >= 0){
                                    utils.click({selector:"#ctl00_cphPageContent_myWizard_cntrlRecurrence_chkSaturday"});
                                }
                            }
                            else if(schedulemeetingdata.table.Frequency[ii] === 'Monthly'){
                                // fill n months
                                utils.fillText({selector:"#ctl00_cphPageContent_myWizard_cntrlRecurrence_txtNMonths", value:schedulemeetingdata.table.Nmonths[ii]});
                                
                                if(schedulemeetingdata.table.MonthlyFrequency[ii] === '3rdSaturday'){
                                    utils.click({selector:"#ctl00_cphPageContent_myWizard_cntrlRecurrence_rdioNthWeekDay[value='rdioNthWeekDay']"});
                                    utils.delay(1000);
                                }
                                else if(schedulemeetingdata.table.MonthlyFrequency[ii] === '20thDay'){
                                    utils.click({selector:"#ctl00_cphPageContent_myWizard_cntrlRecurrence_rdioNthDay[value='rdioNthDay']"});
                                    utils.delay(1000);
                                }   
                            }
                        }

                        // fill end date
                        utils.fillText({selector:"#ctl00_cphPageContent_myWizard_cntrlRecurrence_txtRecurrenceEndDate", value:schedulemeetingdata.table.EndDate[ii]});

                        if(!!schedulemeetingdata.table.AttendeeRegisterMethod[ii]){
                            if(schedulemeetingdata.table.AttendeeRegisterMethod[ii] === 'Separately'){
                                utils.click({selector:"#ctl00_cphPageContent_myWizard_cntrlRecurrence_rdioAttendeePickDate[value='rdioAttendeePickDate']"});
                                utils.delay(1000);  
                            }
                            else if(schedulemeetingdata.table.AttendeeRegisterMethod[ii] === 'OnceForAll'){
                                utils.click({selector:"#ctl00_cphPageContent_myWizard_cntrlRecurrence_rdioAttendeeAll[value='rdioAttendeeAll']"});
                                utils.delay(1000);
                            }
                        }   
                    }

                    // fill attendees
                    utils.fillText({selector:"#ctl00_cphPageContent_myWizard_attendeeList_txtEmailList", value:schedulemeetingdata.table.Attendees[ii]});

                    // fill presenters
                    utils.fillText({selector:"#ctl00_cphPageContent_myWizard_presenterList_txtEmailList", value:schedulemeetingdata.table.Presenters[ii]});

                    // fill email subject
                    utils.fillText({selector:"#ctl00_cphPageContent_myWizard_txtSubject", value:schedulemeetingdata.table.EmailSubject[ii]});
                    
                    // fill email body
                    utils.fillText({selector:".ajax__html_editor_extender_texteditor", value:schedulemeetingdata.table.EmailBody[ii], nonInput:true});
                    
                    // fill audio mode
                    if(!!schedulemeetingdata.table.AudioMode[ii]){
                        if(schedulemeetingdata.table.AudioMode[ii] === 'DiscussionMode'){
                            utils.click({selector:"#ctl00_cphPageContent_myWizard_rbDiscussionMode[value='rbDiscussionMode']"});
                            utils.delay(1000);
                        }
                        else if(schedulemeetingdata.table.AudioMode[ii] === 'ListenMode'){
                            utils.click({selector:"#ctl00_cphPageContent_myWizard_rbListenMode[value='rbListenMode']"});
                            utils.delay(1000);
                        }
                    }

                    // make public settings
                    if(schedulemeetingdata.table.MakeAttendeesPublic[ii] === "True"){
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_chkAttendeeListIsPublicNew"});
                    }
                    
                    if(schedulemeetingdata.table.MakeProfilePublic[ii] === "True"){
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_PublicMeetingFields1_cbxIsPublic"});

                        if(schedulemeetingdata.table.AllowComments[ii] === "True"){
                            utils.click({selector:"#ctl00_cphPageContent_myWizard_PublicMeetingFields1_cbxAllowComments"});
                        }

                        // fill meeting topic
                        utils.fillText({selector:"#ctl00_cphPageContent_myWizard_PublicMeetingFields1_txtMeetingTopic_txtMain", value:schedulemeetingdata.table.MeetingTopic[ii]});

                        // fill brief description
                        utils.fillText({selector:"#ctl00_cphPageContent_myWizard_PublicMeetingFields1_txtMeetingExtDesc_txtMain", value:schedulemeetingdata.table.BriefDescription[ii]});
                    }

                    utils.click({selector:"#StartSendNowButton"});

                    if(!schedulemeetingdata.table.Title[ii] || !!schedulemeetingdata.table.ErrorMessage[ii]){
                        if(!schedulemeetingdata.table.Title[ii]){
                            utils.waitForElementToLoad({selector:"#ctl00_cphPageContent_myWizard_txtMeetingTitleRequired", retries:30, timeout:1000});
                            utils.assertStyleEquals({selector:"#ctl00_cphPageContent_myWizard_txtMeetingTitleRequired", attribute:"visibility", value:"visible"});
                        }
                        if(!!schedulemeetingdata.table.ErrorMessage[ii]){
                            utils.waitForElementToLoad({selector:"#ctl00_cphPageContent_lblError_lblCustomError", retries:30, timeout:1000});
                            utils.assertTextContains({selector:"#ctl00_cphPageContent_lblError_lblCustomError", value:schedulemeetingdata.table.ErrorMessage[ii]});
                        }
                    }
                    else{
                        utils.waitForElementToLoad({selector:"#ctl00_cphPageContent_lblHeader", retries:30, timeout:1000});
                        utils.assertTextEquals({selector:"#ctl00_cphPageContent_lblHeader", value:"Congratulations, your invite has been sent."});
                    }

                    utils.endTest();

                }

                testRun.start(homeUrl, function(t){                        
                        reportbuilder.initWebTestReport(reportOptions.reportContainerSelector, t.testCount);                      
                    }, function(t){
                        reportbuilder.renderRun(t.currentRun);
                        testdataservice.endTest('ScheduleMeetingTest');
                    }, function(t){
                        reportbuilder.renderTest(t.currentTest);
                });
            });

        });

    })

});