define([
  'tester',
  'testdataservice',
  'config',
  'reportbuilder'
], function(
    tester,
    testdataservice,
    config,
    reportbuilder
){

    var baseUrl = config.httpsBaseUrl;

    var deactivateUrl = baseUrl + "AccountManager/DeactivationRequest.aspx";
    var signupUrl = baseUrl + "AccountManager/AnyMeetingSignUp.aspx";
    var logoutUrl = baseUrl + "AccountManager/Logout.aspx";
    
    var reportOptions = {reportContainerSelector:"#test"}

    var testRun;
    

    $(function(){

        testRun = tester.newRun(function(utils){

            testdataservice.startTest('DeactivateTest');

            // DEACTIVATE TESTS
            utils.newTest("DeactivateTest");

            utils.waitForElementToLoad({selector:"#ctl00_cphPageContent_Wizard1_radioListWhyDowngrade_1", retries:30, timeout:1000});
            utils.click({selector:"#ctl00_cphPageContent_Wizard1_radioListWhyDowngrade_1"});
            utils.delay(1000);

            utils.click({selector:"#ctl00_cphPageContent_Wizard1_StartNavigationTemplateContainerID_StartNextButton"});
        
            utils.waitForElementToLoad({selector:"#ctl00_cphPageContent_Wizard1_StepNavigationTemplateContainerID_StepNextButton", retries:30, timeout:1000});
            utils.click({selector:"#ctl00_cphPageContent_Wizard1_StepNavigationTemplateContainerID_StepNextButton"});

            utils.waitForElementToLoad({selector:"#ctl00_cphPageContent_Wizard1_pnlOutreach", retries:30, timeout:1000});
            utils.assertTextContains({selector:"#ctl00_cphPageContent_Wizard1_pnlOutreach", value:"Your account has been de-activated"});


            // sign up page
            utils.loadResource({url:signupUrl});

            // Restore account for subsequent tests
            utils.waitForElementToLoad({selector:"#ctl00_ContentPlaceHolderBody_SignupWidget_txtFirstName_txtInput", retries:30, timeout:1000});

            // fill first name
            utils.fillText({selector:"#ctl00_ContentPlaceHolderBody_SignupWidget_txtFirstName_txtInput", value:"Mark"});
            // fill last name
            utils.fillText({selector:"#ctl00_ContentPlaceHolderBody_SignupWidget_txtLastName_txtInput", value:"Zipagang"});
            // fill email
            utils.fillText({selector:"#ctl00_ContentPlaceHolderBody_SignupWidget_txtEmail_txtInput", value:"11aa@resume.com"});
            // fill password
            utils.fillText({selector:"#ctl00_ContentPlaceHolderBody_SignupWidget_txtPassword_txtInput", value:"hustificado"});
            // fill meeting address
            utils.fillText({selector:"#ctl00_ContentPlaceHolderBody_SignupWidget_txtMeetingURL_txtInput", value: "MeetMe" + testdataservice.generateAlphabeticString()});
            
            utils.setDropdown({selector:'#ctl00_ContentPlaceHolderBody_SignupWidget_ddlTimeZone_ddlTimeZone>option[value="08:00:00"]'});
            
            // set how did you hear
            utils.setDropdown({selector:'#ctl00_ContentPlaceHolderBody_SignupWidget_ddlHowDidYouHear>option[value="Facebook"]'});
            
            utils.click({selector:'#ctl00_ContentPlaceHolderBody_SignupWidget_cbxAgreeService_spnCheckbox'});
            utils.delay(1000);

            // click sign up now button
            utils.click({selector:"#ctl00_ContentPlaceHolderBody_SignupWidget_btnCreateUser"});
            utils.waitForElementToLoad({selector:"#ctl00_cphPageTitle_lblPageHeader", retries:30, timeout:1000});

            // logout
            utils.loadResource({url:logoutUrl});
            utils.waitForElementToLoad({selector:".PageTitle", retries:30, timeout:1000});

            utils.endTest();

        });

        testRun.start(deactivateUrl, function(t){                        
                reportbuilder.initWebTestReport(reportOptions.reportContainerSelector, t.testCount);                      
            }, function(t){
                reportbuilder.renderRun(t.currentRun);
                testdataservice.endTest('DeactivateTest');
            }, function(t){
                reportbuilder.renderTest(t.currentTest);
        });

    })

});