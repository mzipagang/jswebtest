define([
  'tester',
  'testdataservice',
  'config',
  'reportbuilder'
], function(
    tester,
    testdataservice,
    config,
    reportbuilder
){

    var baseUrl = config.httpsBaseUrl;

    var homeUrl = baseUrl + "AccountManager/AnyMeeting.aspx";
    
    var reportOptions = {reportContainerSelector:"#test"}

    var testRun;
    

    $(function(){

        testRun = tester.newRun(function(utils){

            var ii=0;

            testdataservice.getTestData('createmeetingregistrationdata.xls', function(createmeetingregistrationdata){

                testdataservice.startTest('CreateMeetingRegistrationTest');

                utils.waitForElementToLoad({selector:"#ctl00_cphPageContent_btnScheduleMeeting", retries:30, timeout:1000});
                utils.click({selector:"#ctl00_cphPageContent_btnScheduleMeeting"});

                utils.waitForElementToLoad({selector:"#ctl00_cphPageContent_myWizard_txtMeetingTitle", retries:30, timeout:1000}); 
                // fill title
                utils.fillText({selector:"#ctl00_cphPageContent_myWizard_txtMeetingTitle", value:'Test Meeting'});
                
                // fill date
                utils.fillText({selector:"#ctl00_cphPageContent_myWizard_MeetingScheduler1_txtMeetingStartDate", value:'07/20/2015'});

                // fill time
                utils.setDropdown({selector:'#ctl00_cphPageContent_myWizard_MeetingScheduler1_ddlTime>option[value="7:00"]'});
            
                // fill audio mode
                utils.click({selector:"#ctl00_cphPageContent_myWizard_rbDiscussionMode[value='rbDiscussionMode']"});
                utils.delay(1000);

                utils.click({selector:"#ctl00_cphPageContent_myWizard_StartNavigationTemplateContainerID_StartNextButton"});


                // CREATE MEETING REGISTRATION TESTS
                for(ii=0;ii<createmeetingregistrationdata.size;ii++){

                    utils.newTest("CreateMeetingRegistrationTest  " + ((!!createmeetingregistrationdata.table.Description[ii]) ? ("(" + createmeetingregistrationdata.table.Description[ii] + ")") : ""));

                    utils.waitForElementToLoad({selector:".ajax__html_editor_extender_texteditor:lt(1)", retries:30, timeout:1000});
                    // fill header text
                    utils.fillText({selector:".ajax__html_editor_extender_texteditor:lt(1)", value:createmeetingregistrationdata.table.HeaderText[ii], nonInput:true});

                    // address fields
                    if(createmeetingregistrationdata.table.AddressFields[ii].indexOf("Address") >= 0){
                        // select address
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxAddress"})
                    }
                    if(createmeetingregistrationdata.table.AddressFields[ii].indexOf("City") >= 0){
                        // select city
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxCity"})
                    }
                    if(createmeetingregistrationdata.table.AddressFields[ii].indexOf("State") >= 0){
                        // select state
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxState"})
                    }
                    if(createmeetingregistrationdata.table.AddressFields[ii].indexOf("Zip") >= 0){
                        // select zip
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxZip"})
                    }
                    if(createmeetingregistrationdata.table.AddressFields[ii].indexOf("Country") >= 0){
                        // select country
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxCountry"})
                    }
                    if(createmeetingregistrationdata.table.AddressFields[ii].indexOf("Phone") >= 0){
                        // select phone
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxPhone"})
                    }

                    // require address fields
                    if(createmeetingregistrationdata.table.RequireAddressFields[ii].indexOf("Address") >= 0){
                        // select require address
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxRAddress"})
                    }
                    if(createmeetingregistrationdata.table.RequireAddressFields[ii].indexOf("City") >= 0){
                        // select require city
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxRCity"})
                    }
                    if(createmeetingregistrationdata.table.RequireAddressFields[ii].indexOf("State") >= 0){
                        // select require state
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxRState"})
                    }
                    if(createmeetingregistrationdata.table.RequireAddressFields[ii].indexOf("Zip") >= 0){
                        // select require zip
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxRZip"})
                    }
                    if(createmeetingregistrationdata.table.RequireAddressFields[ii].indexOf("Country") >= 0){
                        // select require country
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxRCountry"})
                    }
                    if(createmeetingregistrationdata.table.RequireAddressFields[ii].indexOf("Phone") >= 0){
                        // select require phone
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxRPhone"})
                    }

                    // marketing fields
                    if(createmeetingregistrationdata.table.MarketingFields[ii].indexOf("Organization") >= 0){
                        // select organization
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxOrganization"})
                    }
                    if(createmeetingregistrationdata.table.MarketingFields[ii].indexOf("JobTitle") >= 0){
                        // select job title
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxJobTitle"})
                    }
                    if(createmeetingregistrationdata.table.MarketingFields[ii].indexOf("Industry") >= 0){
                        // select industry
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxIndustry"})
                    }
                    if(createmeetingregistrationdata.table.MarketingFields[ii].indexOf("Comments") >= 0){
                        // select comments
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxComments"})
                    }

                    // require marketing fields
                    if(createmeetingregistrationdata.table.RequireMarketingFields[ii].indexOf("Organization") >= 0){
                        // select require organization
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxROrganization"})
                    }
                    if(createmeetingregistrationdata.table.RequireMarketingFields[ii].indexOf("JobTitle") >= 0){
                        // select require job title
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxRJobTitle"})
                    }
                    if(createmeetingregistrationdata.table.RequireMarketingFields[ii].indexOf("Industry") >= 0){
                        // select require industry
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxRIndustry"})
                    }
                    if(createmeetingregistrationdata.table.RequireMarketingFields[ii].indexOf("Comments") >= 0){
                        // select require comments
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxRComments"})
                    }

                    // custom fields
                    if(createmeetingregistrationdata.table.HasCustomFields[ii] === "True"){
                        utils.runSource({source:'javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("ctl00$cphPageContent$myWizard$CustomFieldsRegistration$btnCreateCustomQuestion$lbMain", "", true, "", "", false, true))'});

                        utils.waitForElementToLoad({selector:"#ctl00_cphPageContent_myWizard_CustomFieldsRegistration_txtQuestion", retries:30, timeout:1000});
                        // fill question
                        utils.fillText({selector:"#ctl00_cphPageContent_myWizard_CustomFieldsRegistration_txtQuestion", value:createmeetingregistrationdata.table.Question[ii]});

                        // require answer
                        if(createmeetingregistrationdata.table.RequireAnswer[ii] === 'True'){
                            utils.click({selector:"#ctl00_cphPageContent_myWizard_CustomFieldsRegistration_cbxRequireCustomQuestion"});
                        }

                        // fill answers
                        var answers = createmeetingregistrationdata.table.Answers[ii].split(',');
                        for(var jj=0;jj<answers.length;jj++){
                            utils.fillText({selector:"#ctl00_cphPageContent_myWizard_CustomFieldsRegistration_txtA" + (jj + 1), value:$.trim(answers[jj])});
                        }

                        utils.click({selector:"#ctl00_cphPageContent_myWizard_CustomFieldsRegistration_btnAdd"});

                        utils.waitForElementToLoad({selector:"#ctl00_cphPageContent_myWizard_CustomFieldsRegistration_gvCustomFields_ctl02_btnEdit", retries:30, timeout:1000});
                    }

                    utils.waitForElementToLoad({selector:".ajax__html_editor_extender_texteditor:lt(2):gt(0)", retries:30, timeout:1000});
                    // fill footer text
                    utils.fillText({selector:".ajax__html_editor_extender_texteditor:lt(2):gt(0)", value:createmeetingregistrationdata.table.FooterText[ii], nonInput:true});

                    // fill terms of service
                    utils.fillText({selector:".ajax__html_editor_extender_texteditor:gt(1)", value:createmeetingregistrationdata.table.TermsOfService[ii], nonInput:true});

                    // fill max registrants
                    utils.fillText({selector:"#ctl00_cphPageContent_myWizard_txtMaximumRegistrants", value:createmeetingregistrationdata.table.MaxRegistrants[ii]});

                    // password protect
                    if(createmeetingregistrationdata.table.PasswordProtect[ii] === 'True'){
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_rblRegistrationSecurity_1"});
                    }
                    else{
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_rblRegistrationSecurity_0"});
                    }

                    utils.click({selector:"#ctl00_cphPageContent_myWizard_StepNavigationTemplateContainerID_StepNextButton"});

                    if(isNaN(parseInt(createmeetingregistrationdata.table.MaxRegistrants[ii]))){
                        utils.waitForElementToLoad({selector:"#ctl00_cphPageContent_lblError_lblCustomError", retries:30, timeout:1000});
                        utils.assertTextContains({selector:"#ctl00_cphPageContent_lblError_lblCustomError", value:"The Maximum Registrants field must be blank or contain only numbers."});
                    }
                    else{
                        utils.waitForElementToLoad({selector:"#ctl00_cphPageContent_myWizard_cbxSurveyShowPresenter", retries:30, timeout:1000});
                        utils.assertTextContains({selector:"#ctl00_cphPageTitle_lblEventTitle", value:"Create Survey"});

                        utils.click({selector:"#ctl00_cphPageContent_myWizard_StepNavigationTemplateContainerID_StepNextButton"});
                        utils.waitForElementToLoad({selector:"#ctl00_cphPageContent_myWizard_Registration1_pnlRegistration", retries:30, timeout:1000});

                        utils.click({selector:"#FinishButton"});
                        utils.waitForElementToLoad({selector:"#ctl00_cphPageContent_pnlFinish", retries:30, timeout:1000});
                    }
                    
                    utils.endTest();

                }

                 testRun.start(homeUrl, function(t){                        
                        reportbuilder.initWebTestReport(reportOptions.reportContainerSelector, t.testCount);                      
                    }, function(t){
                        reportbuilder.renderRun(t.currentRun);
                        testdataservice.endTest('CreateMeetingRegistrationTest');
                    }, function(t){
                        reportbuilder.renderTest(t.currentTest);
                });

            });

        });

    })

});