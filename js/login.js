define([
  'tester',
  'testdataservice',
  'config',
  'reportbuilder'
], function(
    tester,
    testdataservice,
    config,
    reportbuilder
){
    var baseUrl = config.httpsBaseUrl;

    var loginUrl = baseUrl + "AccountManager/Login.aspx";
    
    var reportOptions = {reportContainerSelector:"#test"}

    var testRun;


    $(function(){

        testRun = tester.newRun(function(utils){

            var ii=0;

            testdataservice.getTestData('logindata.xls', function(logindata){

                testdataservice.startTest('LoginTest');

                // LOGIN TESTS
                for(ii=0;ii<logindata.size;ii++){

                    utils.newTest("LoginTest  " + ((!!logindata.table.Description[ii]) ? ("(" + logindata.table.Description[ii] + ")") : ""));

                    utils.loadResource({url:loginUrl});    

                    utils.waitForElementToLoad({selector:"#ctl00_cphPageContent_txtUserEmail", retries:30, timeout:1000});

                    // fill email
                    utils.fillText({selector:"#ctl00_cphPageContent_txtUserEmail", value:logindata.table.Email[ii]});
                    // fill password
                    utils.fillText({selector:"#ctl00_cphPageContent_txtUserPassword", value:logindata.table.Password[ii]});

                    if(logindata.table.KeepMeLoggedIn[ii] === 'True'){
                        // set keep me logged in box
                        utils.setCheckbox({selector:'#ctl00_cphPageContent_cbxRememberMe', isChecked:true});
                    }
                    else{
                        utils.setCheckbox({selector:'#ctl00_cphPageContent_cbxRememberMe', isChecked:false});   
                    }

                    // click login button
                    utils.runSource({source:"javascript:__doPostBack('ctl00$cphPageContent$btnLogin$lbMain','');"});

                    // asserts
                    if(logindata.table.ErrorMessage[ii] === 'We could not log you in using that email and password combination'){
                        utils.waitForElementToLoad({selector:"#ctl00_cphPageContent_vsMain>ul>li", retries:30, timeout:1000});
                        utils.assertTextEquals({selector:"#ctl00_cphPageContent_vsMain>ul>li", value:logindata.table.ErrorMessage[ii]});
                    }
                    else if(logindata.table.ErrorMessage[ii] === 'Please enter a Password'){
                        utils.waitForElementToLoad({selector:"#ctl00_cphPageContent_vsMain>ul>li", retries:30, timeout:1000});
                        utils.assertTextEquals({selector:"#ctl00_cphPageContent_vsMain>ul>li", value:logindata.table.ErrorMessage[ii]});
                    }
                    else if(logindata.table.ErrorMessage[ii] === 'Please enter your E-mail Address'){
                        utils.waitForElementToLoad({selector:"#ctl00_cphPageContent_vsMain>ul>li", retries:30, timeout:1000});
                        utils.assertTextEquals({selector:"#ctl00_cphPageContent_vsMain>ul>li", value:logindata.table.ErrorMessage[ii]});
                    }
                    // was able to login successfully
                    else{
                        utils.delay(5000);                         
                    }

                    utils.endTest();

                }

                testRun.start(loginUrl, function(t){                        
                        reportbuilder.initWebTestReport(reportOptions.reportContainerSelector, t.testCount);                      
                    }, function(t){
                        reportbuilder.renderRun(t.currentRun);
                        testdataservice.endTest('LoginTest');
                    }, function(t){
                        reportbuilder.renderTest(t.currentTest);
                });

            });
        });

    });

});