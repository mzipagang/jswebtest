// Filename: main.js
// Require.js allows us to configure shortcut alias
//
require.config({

	paths: {
		jquery: 'libs/jquery/jquery',
		config:'libs/jswebtest/config',
		tester:'libs/jswebtest/tester',
		crawler:'libs/jswebtest/crawler',
		reportbuilder:'libs/jswebtest/reportbuilder',
		testdataservice:'libs/jswebtest/testdataservice'
	},
  
  waitSeconds:30

});

// load page-specific js
if(window.location.href.indexOf("index.html")>0){
	require(['index']);
}
if(window.location.href.indexOf("signup.html")>0){
	require(['signup']);
}
if(window.location.href.indexOf("login.html")>0){
	require(['login']);
}
if(window.location.href.indexOf("logout.html")>0){
	require(['logout']);
}
if(window.location.href.indexOf("schedulemeeting.html")>0){
	require(['schedulemeeting']);
}
if(window.location.href.indexOf("startmeeting.html")>0){	
	require(['startmeeting']);
}
if(window.location.href.indexOf("upgradepath.html")>0){
	require(['upgradepath']);
}
if(window.location.href.indexOf("downgrade.html")>0){
	require(['downgrade']);
}
if(window.location.href.indexOf("deactivate.html")>0){
	require(['deactivate']);
}
if(window.location.href.indexOf("publicprofile.html")>0){
	require(['publicprofile']);
}
if(window.location.href.indexOf("createmeetingregistration.html")>0){
	require(['createmeetingregistration']);
}
if(window.location.href.indexOf("createmeetingsurvey.html")>0){
	require(['createmeetingsurvey']);
}
if(window.location.href.indexOf("completemeetingsurvey.html")>0){
	require(['completemeetingsurvey']);
}
if(window.location.href.indexOf("completemeetingregistration.html")>0){
	require(['completemeetingregistration']);
}
if(window.location.href.indexOf("linkcrawler.html")>0){
	require(['linkcrawler']);
}