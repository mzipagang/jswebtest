define([
  'tester',
  'testdataservice',
  'config',
  'reportbuilder'
], function(
    tester,
    testdataservice,
    config,
    reportbuilder
){

    var baseUrl = config.httpsBaseUrl;

    var loginUrl = baseUrl + "AccountManager/Login.aspx";
    var homeUrl = baseUrl + "AccountManager/AnyMeeting.aspx";
    
    var reportOptions = {reportContainerSelector:"#test"}

    var testRun;
    

    $(function(){

        testRun = tester.newRun(function(utils){

            var ii=0;

            testdataservice.getTestData('upgradepathdata.xls', function(upgradepathdata){

                testdataservice.startTest('UpgradePathTest');

                utils.loadResource({url:loginUrl});    

                utils.waitForElementToLoad({selector:"#ctl00_cphPageContent_txtUserEmail", retries:30, timeout:1000});

                // fill email
                utils.fillText({selector:"#ctl00_cphPageContent_txtUserEmail", value:"11aa@resume.com"});
                // fill password
                utils.fillText({selector:"#ctl00_cphPageContent_txtUserPassword", value:"hustificado"});

                // set keep me logged in box
                utils.setCheckbox({selector:'#ctl00_cphPageContent_cbxRememberMe', isChecked:true});

                // click login button
                utils.runSource({source:"javascript:__doPostBack('ctl00$cphPageContent$btnLogin$lbMain','');"});
                utils.delay(5000);

                // UPGRADE PATH TESTS
                for(ii=0;ii<upgradepathdata.size;ii++){

                    utils.newTest("UpgradePathTest  " + ((!!upgradepathdata.table.Description[ii]) ? ("(" + upgradepathdata.table.Description[ii] + ")") : ""));

                    if(ii>0){
                        utils.loadResource({url:homeUrl});   
                    } 

                    utils.waitForElementToLoad({selector:"#btnRemoveAds", retries:30, timeout:1000});
                    utils.click({selector:"#btnRemoveAds"});

                    utils.waitForElementToLoad({selector:".SubPlan-25 .SubPlan-Button", retries:30, timeout:1000}); 
                    utils.click({selector:".SubPlan-25 .SubPlan-Button"});
                    
                    utils.waitForElementToLoad({iframeSelector:"#frameSignup", selector:"label[for='planMonthly']", retries:60, timeout:1000});
                    
                    if(upgradepathdata.table.Interval[ii] === "Monthly"){
                        // click on Monthly                    
                        utils.click({iframeSelector:"#frameSignup", selector:"label[for='planMonthly']"});
                    }
                    else if(upgradepathdata.table.Interval[ii] === "Yearly"){
                        // click on Yearly                    
                        utils.click({iframeSelector:"#frameSignup", selector:"label[for='planAnnual']"});   
                    }

                    utils.waitForElementToLoad({iframeSelector:"#frameSignup", selector:".cvv input", retries:60, timeout:1000});
                    // fill coupon code
                    utils.fillText({iframeSelector:"#frameSignup", selector:".coupon_code input", value:upgradepathdata.table.CouponCode[ii]});                    
                    
                    // fill first name
                    utils.fillText({iframeSelector:"#frameSignup", selector:".first_name input", value:upgradepathdata.table.FirstName[ii]});
                    
                    // fill last name
                    utils.fillText({iframeSelector:"#frameSignup", selector:".last_name input", value:upgradepathdata.table.LastName[ii]});

                    // fill email
                    utils.fillText({iframeSelector:"#frameSignup", selector:".email input", value:upgradepathdata.table.Email[ii]});

                    // fill phone
                    utils.fillText({iframeSelector:"#frameSignup", selector:".phone input", value:upgradepathdata.table.Phone[ii]});

                    // fill cc
                    utils.fillText({iframeSelector:"#frameSignup", selector:".card_number input", value:upgradepathdata.table.CC[ii]});

                    // fill cvv
                    utils.fillText({iframeSelector:"#frameSignup", selector:".cvv input", value:upgradepathdata.table.CVV[ii]});

                    // set month
                    utils.setDropdown({iframeSelector:"#frameSignup", selector:'.month select>option[value="' + upgradepathdata.table.CCMonth[ii] + '"]'});

                    // set year
                    utils.setDropdown({iframeSelector:"#frameSignup", selector:'.year select>option[name="' + upgradepathdata.table.CCYear[ii] + '"]'});   

                    // fill street 1
                    utils.fillText({iframeSelector:"#frameSignup", selector:".address1 input", value:upgradepathdata.table.Street1[ii]});

                    // fill street 2
                    utils.fillText({iframeSelector:"#frameSignup", selector:".address2 input", value:upgradepathdata.table.Street2[ii]});

                    // fill city
                    utils.fillText({iframeSelector:"#frameSignup", selector:".city input", value:upgradepathdata.table.City[ii]});

                    // fill state
                    utils.fillText({iframeSelector:"#frameSignup", selector:".state input", value:upgradepathdata.table.State[ii]});

                    // fill zip
                    utils.fillText({iframeSelector:"#frameSignup", selector:".zip input", value:upgradepathdata.table.Zip[ii]});

                    // set country
                    utils.setDropdown({iframeSelector:"#frameSignup", selector:'.country select>option[value="' + upgradepathdata.table.Country[ii] + '"]'});

                    // click on 'Subscribe'                    
                    utils.click({iframeSelector:"#frameSignup", selector:".footer button"});
                    utils.delay(2000);

                    if(upgradepathdata.table.IsValid[ii] === 'False'){
                        utils.assertElementExists({iframeSelector:"#frameSignup", selector:".error"});
                    }
                    else if(upgradepathdata.table.IsValid[ii] === 'DENIED'){
                        utils.waitForElementToLoad({iframeSelector:"#frameSignup", selector:".error", retries:30, timeout:1000});
                        utils.assertTextContains({iframeSelector:"#frameSignup", selector:".error", value:"Your card was declined. In order to resolve the issue, you will need to contact your bank."});
                    }
                    else if(upgradepathdata.table.IsValid[ii] === 'True'){
                        utils.waitForElementToLoad({iframeSelector:"#frameSignup", selector:"#Content h3", retries:30, timeout:1000});
                        utils.assertTextContains({iframeSelector:"#frameSignup", selector:"#Content h3", value:"Success!"});
                    }

                    utils.endTest();

                }

                testRun.start(homeUrl, function(t){                        
                        reportbuilder.initWebTestReport(reportOptions.reportContainerSelector, t.testCount);                      
                    }, function(t){
                        reportbuilder.renderRun(t.currentRun);
                        testdataservice.endTest('UpgradePathTest');
                    }, function(t){
                        reportbuilder.renderTest(t.currentTest);
                });

            });

        });

    })

});