define([
  'tester',
  'testdataservice',
  'config',
  'reportbuilder'
], function(
    tester,
    testdataservice,
    config,
    reportbuilder
){

    var baseUrl = config.httpsBaseUrl;

    var homeUrl = baseUrl + "AccountManager/AnyMeeting.aspx";
    
    var reportOptions = {reportContainerSelector:"#test"}

    var testRun;
    

    $(function(){

        testRun = tester.newRun(function(utils){

            var ii=0;

            testdataservice.getTestData('createmeetingsurveydata.xls', function(createmeetingsurveydata){

                testdataservice.startTest('CreateMeetingSurveyTest');

                utils.waitForElementToLoad({selector:"#ctl00_cphPageContent_btnScheduleMeeting", retries:30, timeout:1000});
                utils.click({selector:"#ctl00_cphPageContent_btnScheduleMeeting"});

                utils.waitForElementToLoad({selector:"#ctl00_cphPageContent_myWizard_txtMeetingTitle", retries:30, timeout:1000}); 
                // fill title
                utils.fillText({selector:"#ctl00_cphPageContent_myWizard_txtMeetingTitle", value:'Test Meeting'});
                
                // fill date
                utils.fillText({selector:"#ctl00_cphPageContent_myWizard_MeetingScheduler1_txtMeetingStartDate", value:'07/20/2015'});

                // fill time
                utils.setDropdown({selector:'#ctl00_cphPageContent_myWizard_MeetingScheduler1_ddlTime>option[value="8:00"]'});
            
                // fill audio mode
                utils.click({selector:"#ctl00_cphPageContent_myWizard_rbDiscussionMode[value='rbDiscussionMode']"});
                utils.delay(1000);

                // go to meeting registration page
                utils.click({selector:"#ctl00_cphPageContent_myWizard_StartNavigationTemplateContainerID_StartNextButton"});
                
                utils.waitForElementToLoad({selector:".ajax__html_editor_extender_texteditor:lt(2):gt(0)", retries:30, timeout:1000});

                // go to meeting survey page
                utils.click({selector:"#ctl00_cphPageContent_myWizard_StepNavigationTemplateContainerID_StepNextButton"});


                // CREATE MEETING SURVEY TESTS
                for(ii=0;ii<createmeetingsurveydata.size;ii++){

                    utils.newTest("CreateMeetingSurveyTest  " + ((!!createmeetingsurveydata.table.Description[ii]) ? ("(" + createmeetingsurveydata.table.Description[ii] + ")") : ""));

                    utils.waitForElementToLoad({selector:".ajax__html_editor_extender_texteditor:lt(1)", retries:30, timeout:1000});
                    // fill header text
                    utils.fillText({selector:".ajax__html_editor_extender_texteditor:lt(1)", value:createmeetingsurveydata.table.HeaderText[ii], nonInput:true});

                    // address fields
                    if(createmeetingsurveydata.table.AddressFields[ii].indexOf("Address") >= 0){
                        // select address
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxSurveyShowAddress"})
                    }
                    if(createmeetingsurveydata.table.AddressFields[ii].indexOf("City") >= 0){
                        // select city
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxSurveyShowCity"})
                    }
                    if(createmeetingsurveydata.table.AddressFields[ii].indexOf("State") >= 0){
                        // select state
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxSurveyShowState"})
                    }
                    if(createmeetingsurveydata.table.AddressFields[ii].indexOf("Zip") >= 0){
                        // select zip
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxSurveyShowZip"})
                    }
                    if(createmeetingsurveydata.table.AddressFields[ii].indexOf("Country") >= 0){
                        // select country
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxSurveyShowCountry"})
                    }
                    if(createmeetingsurveydata.table.AddressFields[ii].indexOf("Phone") >= 0){
                        // select phone
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxSurveyShowPhone"})
                    }

                    // require address fields
                    if(createmeetingsurveydata.table.RequireAddressFields[ii].indexOf("Address") >= 0){
                        // select require address
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxSurveyReqAddress"})
                    }
                    if(createmeetingsurveydata.table.RequireAddressFields[ii].indexOf("City") >= 0){
                        // select require city
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxSurveyReqCity"})
                    }
                    if(createmeetingsurveydata.table.RequireAddressFields[ii].indexOf("State") >= 0){
                        // select require state
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxSurveyReqState"})
                    }
                    if(createmeetingsurveydata.table.RequireAddressFields[ii].indexOf("Zip") >= 0){
                        // select require zip
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxSurveyReqZip"})
                    }
                    if(createmeetingsurveydata.table.RequireAddressFields[ii].indexOf("Country") >= 0){
                        // select require country
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxSurveyReqCountry"})
                    }
                    if(createmeetingsurveydata.table.RequireAddressFields[ii].indexOf("Phone") >= 0){
                        // select require phone
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxSurveyReqPhone"})
                    }

                    // marketing fields
                    if(createmeetingsurveydata.table.MarketingFields[ii].indexOf("Organization") >= 0){
                        // select organization
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxSurveyShowOrganization"})
                    }
                    if(createmeetingsurveydata.table.MarketingFields[ii].indexOf("JobTitle") >= 0){
                        // select job title
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxSurveyShowJobTitle"})
                    }
                    if(createmeetingsurveydata.table.MarketingFields[ii].indexOf("Industry") >= 0){
                        // select industry
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxSurveyShowIndustry"})
                    }
                    if(createmeetingsurveydata.table.MarketingFields[ii].indexOf("Comments") >= 0){
                        // select comments
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxSurveyShowComments"})
                    }
                    if(createmeetingsurveydata.table.MarketingFields[ii].indexOf("FollowUp") >= 0){
                        // select follow up
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxSurveyShowFollowUp"})
                    }

                    // require marketing fields
                    if(createmeetingsurveydata.table.RequireMarketingFields[ii].indexOf("Organization") >= 0){
                        // select require organization
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxSurveyReqOrganization"})
                    }
                    if(createmeetingsurveydata.table.RequireMarketingFields[ii].indexOf("JobTitle") >= 0){
                        // select require job title
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxSurveyReqJobTitle"})
                    }
                    if(createmeetingsurveydata.table.RequireMarketingFields[ii].indexOf("Industry") >= 0){
                        // select require industry
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxSurveyReqIndustry"})
                    }
                    if(createmeetingsurveydata.table.RequireMarketingFields[ii].indexOf("Comments") >= 0){
                        // select require comments
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxSurveyReqComments"})
                    }
                    if(createmeetingsurveydata.table.RequireMarketingFields[ii].indexOf("FollowUp") >= 0){
                        // select require follow up
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxSurveyReqFollowUp"})
                    }

                    // rating fields
                    if(createmeetingsurveydata.table.RatingFields[ii].indexOf("Presenter") >= 0){
                        // select presenter
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxSurveyShowPresenter"})
                    }
                    if(createmeetingsurveydata.table.RatingFields[ii].indexOf("Content") >= 0){
                        // select content
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxSurveyShowContent"})
                    }
                    if(createmeetingsurveydata.table.RatingFields[ii].indexOf("Technology") >= 0){
                        // select technology
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxSurveyShowTechnology"})
                    }

                    // require rating fields
                    if(createmeetingsurveydata.table.RequireRatingFields[ii].indexOf("Presenter") >= 0){
                        // select require presenter
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxSurveyReqPresenter"})
                    }
                    if(createmeetingsurveydata.table.RequireRatingFields[ii].indexOf("Content") >= 0){
                        // select require content
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxSurveyReqContent"})
                    }
                    if(createmeetingsurveydata.table.RequireRatingFields[ii].indexOf("Technology") >= 0){
                        // select require technology
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxSurveyReqTechnology"})
                    }

                    if(createmeetingsurveydata.table.HasCustomFields[ii] === 'True'){
                        // enable scoring
                        if(createmeetingsurveydata.table.EnableScore[ii] === 'True'){
                            utils.click({selector:"#ctl00_cphPageContent_myWizard_CustomFieldsSurvey_cbxEnableScoringCheckBox"});
                            utils.delay(1000);

                            if(createmeetingsurveydata.table.ScoreOptions[ii].indexOf('ShowPassFail') >= 0){
                                utils.click({selector:"#ctl00_cphPageContent_myWizard_CustomFieldsSurvey_cbPassFail"});
                                utils.waitForElementToLoad({selector:"#ctl00_cphPageContent_myWizard_CustomFieldsSurvey_tbPassPercentage", retries:30, timeout:1000});
                                // fill passing score
                                utils.fillText({selector:"#ctl00_cphPageContent_myWizard_CustomFieldsSurvey_tbPassPercentage", value:createmeetingsurveydata.table.PassPercentage[ii]});

                                if(createmeetingsurveydata.table.SurveyOutcome[ii] === 'Text'){
                                    utils.click({selector:"#ctl00_cphPageContent_myWizard_CustomFieldsSurvey_rblDisplayScore_0"});
                                    utils.waitForElementToLoad({selector:"#ctl00_cphPageContent_myWizard_CustomFieldsSurvey_passFailText .ajax__html_editor_extender_texteditor:lt(1)", retries:30, timeout:1000});
                                    // fill pass text
                                    utils.fillText({selector:"#ctl00_cphPageContent_myWizard_CustomFieldsSurvey_passFailText .ajax__html_editor_extender_texteditor:lt(1)", value:createmeetingsurveydata.table.PassText[ii]});

                                    // fill fail text
                                    utils.fillText({selector:"#ctl00_cphPageContent_myWizard_CustomFieldsSurvey_passFailText .ajax__html_editor_extender_texteditor:gt(0)", value:createmeetingsurveydata.table.FailText[ii]});
                                }
                                else if(createmeetingsurveydata.table.SurveyOutcome[ii] === 'Url'){
                                    utils.click({selector:"#ctl00_cphPageContent_myWizard_CustomFieldsSurvey_rblDisplayScore_1"});
                                    utils.waitForElementToLoad({selector:"#ctl00_cphPageContent_myWizard_CustomFieldsSurvey_tbPassUrl", retries:30, timeout:1000});
                                    // fill pass url
                                    utils.fillText({selector:"#ctl00_cphPageContent_myWizard_CustomFieldsSurvey_tbPassUrl", value:createmeetingsurveydata.table.PassUrl[ii]});

                                    // fill fail url
                                    utils.fillText({selector:"#ctl00_cphPageContent_myWizard_CustomFieldsSurvey_tbFailUrl", value:createmeetingsurveydata.table.FailUrl[ii]});
                                }
                                else if(createmeetingsurveydata.table.SurveyOutcome[ii] === 'None'){
                                    utils.click({selector:"#ctl00_cphPageContent_myWizard_CustomFieldsSurvey_rblDisplayScore_2"});
                                }
                            }
                            if(createmeetingsurveydata.table.ScoreOptions[ii].indexOf('ShowPercentCorrect') >= 0){
                                utils.click({selector:"#ctl00_cphPageContent_myWizard_CustomFieldsSurvey_cbPercentageCorrect"});
                            }
                            if(createmeetingsurveydata.table.ScoreOptions[ii].indexOf('ShowAnswers') >= 0){
                                utils.click({selector:"#ctl00_cphPageContent_myWizard_CustomFieldsSurvey_cbAnswers"});
                            }
                            if(createmeetingsurveydata.table.ScoreOptions[ii].indexOf('OneSubmissionOnly') >= 0){
                                utils.click({selector:"#ctl00_cphPageContent_myWizard_CustomFieldsSurvey_cbLimitSurveyTakers"});
                            }
                        }

                        // create question
                        utils.runSource({source:'javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("ctl00$cphPageContent$myWizard$CustomFieldsSurvey$btnCreateCustomQuestion$lbMain", "", true, "", "", false, true));'});

                        utils.waitForElementToLoad({selector:"#ctl00_cphPageContent_myWizard_CustomFieldsSurvey_txtQuestion", retries:30, timeout:1000});
                        // fill question
                        utils.fillText({selector:"#ctl00_cphPageContent_myWizard_CustomFieldsSurvey_txtQuestion", value:createmeetingsurveydata.table.Question[ii]});

                        // require answer
                        if(createmeetingsurveydata.table.RequireAnswer[ii] === 'True'){
                            utils.click({selector:"#ctl00_cphPageContent_myWizard_CustomFieldsSurvey_cbxRequireCustomQuestion"});
                        }

                        if(createmeetingsurveydata.table.EnableScore[ii] === 'True'){
                            // get correct answers
                            var correctAnswerList = createmeetingsurveydata.table.CorrectAnswers[ii].split(',');
                            var correctAnswers = {};
                            for(var kk=0;kk<correctAnswerList.length;kk++){
                                correctAnswers[$.trim(correctAnswerList[kk])] = true;
                            }
                        }

                        // fill answers, mark correct answers
                        var answers = createmeetingsurveydata.table.Answers[ii].split(',');
                        for(var jj=0;jj<answers.length;jj++){
                            utils.fillText({selector:"#ctl00_cphPageContent_myWizard_CustomFieldsSurvey_txtA" + (jj + 1), value:$.trim(answers[jj])});

                            if(createmeetingsurveydata.table.EnableScore[ii] === 'True'){
                                if(!!correctAnswers[$.trim(answers[jj])]){
                                    utils.click({selector:"#ctl00_cphPageContent_myWizard_CustomFieldsSurvey_rbAnswerPoints" + (jj + 1)});
                                }
                            }
                        }

                        utils.click({selector:"#ctl00_cphPageContent_myWizard_CustomFieldsSurvey_btnAdd"});

                        utils.waitForElementToLoad({selector:"#ctl00_cphPageContent_myWizard_CustomFieldsSurvey_gvCustomFields_ctl02_btnEdit", retries:30, timeout:1000});
                    }

                    utils.waitForElementToLoad({selector:".ajax__html_editor_extender_texteditor:gt(0)", retries:30, timeout:1000});
                    // fill footer text
                    utils.fillText({selector:".ajax__html_editor_extender_texteditor:gt(0)", value:createmeetingsurveydata.table.FooterText[ii], nonInput:true});

                    if(createmeetingsurveydata.table.EmailNotification[ii] === 'True'){
                        utils.click({selector:"#ctl00_cphPageContent_myWizard_cbxSurveyCompletedNotification"});
                    }

                    utils.fillText({selector:"#ctl00_cphPageContent_myWizard_txtSurveyExitURL", value:createmeetingsurveydata.table.SurveyCompleteUrl[ii]});

                    utils.click({selector:"#ctl00_cphPageContent_myWizard_StepNavigationTemplateContainerID_StepNextButton"});
                    
                    utils.waitForElementToLoad({selector:"#ctl00_cphPageContent_myWizard_Registration1_pnlRegistration", retries:30, timeout:1000});
                    utils.assertTextContains({selector:"#ctl00_cphPageTitle_lblEventTitle", value:"Preview Your Work"});

                    utils.click({selector:"#FinishButton"});
                    utils.waitForElementToLoad({selector:"#ctl00_cphPageContent_pnlFinish", retries:30, timeout:1000});

                    utils.endTest();

                }

                 testRun.start(homeUrl, function(t){                        
                        reportbuilder.initWebTestReport(reportOptions.reportContainerSelector, t.testCount);                      
                    }, function(t){
                        reportbuilder.renderRun(t.currentRun);
                        testdataservice.endTest('CreateMeetingSurveyTest');
                    }, function(t){
                        reportbuilder.renderTest(t.currentTest);
                });

            });

        });

    })

});