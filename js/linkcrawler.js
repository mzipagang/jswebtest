define([
  'crawler', 
  'config',
  'reportbuilder'
], function(
    crawler,
    config,
    reportbuilder
){

    $(function(){

    	var startUrl = "http://www.anymeeting-staging.com";
        var baseUrl = config.httpBaseUrl;

        var reportOptions = {reportContainerSelector:"#test"}

        $("#endRun").prop("disabled", true);
        $("#startRun").click(function(){
	        crawler.start(baseUrl, startUrl, 
                ['javascript:void(null);','javascript:ShowTerms();','javascript:;','/AccountManager/Login.aspx','/Support.aspx','/AccountManager/AnyMeetingUser.aspx'], 
                function(){                    
                    reportbuilder.initLinkCrawlReport(reportOptions.reportContainerSelector);                  
                },
                function(url, result, timeElapsedSec){
                    reportbuilder.renderCrawl(url, result, timeElapsedSec);
                },
                function(){
	            $("#startRun").prop("disabled", false);
	            $("#endRun").prop("disabled", true);
	        });

	        $(this).prop("disabled", true);
            $("#endRun").prop("disabled", false);
	    });

	    $("#endRun").click(function(){
            crawler.stop();
            $("#startRun").prop("disabled", false);
            $("#endRun").prop("disabled", true);
        });
    })

});