define([
  'tester',
  'testdataservice',
  'config',
  'reportbuilder'
], function(
    tester,
    testdataservice,
    config,
    reportbuilder
){
    var baseUrl = config.httpsBaseUrl;

    var loginUrl = baseUrl + "AccountManager/Login.aspx";
    var logoutUrl = baseUrl + "AccountManager/Logout.aspx";
    
    var reportOptions = {reportContainerSelector:"#test"}

    var testRun;


    $(function(){

        testRun = tester.newRun(function(utils){

            testdataservice.startTest('LogoutTest');

            // LOGOUT TESTS
            utils.newTest("LogoutTest");

            utils.waitForElementToLoad({selector:"#ctl00_cphPageTitle_lblUserEmail", retries:30, timeout:1000});

            // still logged in from previous login test
            utils.assertAddressBarContains({url:"www.anymeeting-staging.com/AccountManager/AnyMeeting.aspx"});

            utils.loadResource({url:logoutUrl});    

            utils.waitForElementToLoad({selector:".PageTitle", retries:30, timeout:1000});
            utils.assertTextContains({selector:".PageTitle", value:"You've been logged out."});

            utils.endTest();

        });


        testRun.start(loginUrl, function(t){                        
                reportbuilder.initWebTestReport(reportOptions.reportContainerSelector, t.testCount);                      
            }, function(t){
                reportbuilder.renderRun(t.currentRun);
                testdataservice.endTest('LogoutTest');
            }, function(t){
                reportbuilder.renderTest(t.currentTest);
        });

    });

});