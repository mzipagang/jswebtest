define([
  'tester',
  'testdataservice',
  'config',
  'reportbuilder'
], function(
    tester,
    testdataservice,
    config,
    reportbuilder
){

    var baseUrl = config.httpsBaseUrl;

    var homeUrl = baseUrl + "AccountManager/AnyMeeting.aspx";
    
    var reportOptions = {reportContainerSelector:"#test"}

    var testRun;
    

    $(function(){

        testRun = tester.newRun(function(utils){

            var ii=0;

            testdataservice.getTestData('startmeetingdata.xls', function(startmeetingdata){

                testdataservice.startTest('StartMeetingTest');

                // START MEETING TESTS
                for(ii=0;ii<startmeetingdata.size;ii++){

                    utils.newTest("StartMeetingTest  " + ((!!startmeetingdata.table.Description[ii]) ? ("(" + startmeetingdata.table.Description[ii] + ")") : ""));

                    if(ii === 0){
                        utils.waitForElementToLoad({selector:"#ctl00_cphPageContent_btnOpenStartMeetingModal", retries:30, timeout:1000});
                        utils.click({selector:"#ctl00_cphPageContent_btnOpenStartMeetingModal"});
                    }

                    utils.waitForElementToLoad({selector:"#txtTitle", retries:30, timeout:1000});
                    // fill title
                    utils.fillText({selector:"#txtTitle", value:startmeetingdata.table.Title[ii]});
                    
                    // fill attendees list
                    utils.fillText({selector:"#ctl00_cphPageContent_attendeeList_txtEmailList", value:startmeetingdata.table.Attendees[ii]});

                    // fill optional message                
                    utils.fillText({selector:"#ctl00_cphPageContent_txtAdhocMessage", value:startmeetingdata.table.Message[ii]});

                    // fill audio mode
                    if(!!startmeetingdata.table.AudioMode[ii]){
                        if(startmeetingdata.table.AudioMode[ii] === 'DiscussionMode'){
                            utils.click({selector:"#ctl00_cphPageContent_rbDiscussionMode[value='rbDiscussionMode']"});
                            utils.delay(1000);
                        }
                        else if(startmeetingdata.table.AudioMode[ii] === 'ListenMode'){
                            utils.click({selector:"#ctl00_cphPageContent_rbListenMode[value='rbListenMode']"});
                            utils.delay(1000);
                        }
                    }

                    utils.click({selector:"#ctl00_cphPageContent_btnStart"});

                    if(!startmeetingdata.table.Title[ii]){
                        utils.assertAlertDisplayed({errMsg:"No Title alert box didn't show up"});

                        utils.delay(1000);
                    }
                    else{
                        utils.waitForElementToLoad({selector:".newbutton[onclick='doConnectAnyway()']", retries:30, timeout:1000});
                        // click connect anyway button
                        utils.click({selector:".newbutton[onclick='doConnectAnyway()']"});

                        utils.waitForElementToLoad({selector:"#am", retries:30, timeout:1000});
                        utils.assertElementExists({selector:"#am"});
                        utils.delay(10000);

                        utils.loadResource({url:homeUrl});

                        utils.waitForElementToLoad({selector:"#ctl00_cphPageContent_tabsMain_tpnlUpcoming_gvUpcomingInvitations_ctl02_btnGvEditMeeting", retries:30, timeout:1000});
                        // view meeting details
                        utils.runSource({source:"javascript:__doPostBack('ctl00$cphPageContent$tabsMain$tpnlUpcoming$gvUpcomingInvitations$ctl02$btnGvEditMeeting','')"});

                        utils.waitForElementToLoad({selector:"#ctl00_cphPageTitle_LinkButton1", retries:30, timeout:1000});
                        // end now
                        utils.runSource({source:'javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("ctl00$cphPageTitle$LinkButton1", "", true, "", "", false, true));'});

                        utils.waitForElementToLoad({selector:"#__tab_ctl00_cphPageContent_tcMain_tpReports", retries:30, timeout:1000});
                        // click reports tab
                        utils.click({selector:"#__tab_ctl00_cphPageContent_tcMain_tpReports"});

                        utils.waitForElementToLoad({selector:"#ctl00_cphPageContent_tcMain_tpReports_lbtnExportSessionAttendance", retries:30, timeout:1000});
                        // assert report link is there
                        utils.assertElementExists({selector:"#ctl00_cphPageContent_tcMain_tpReports_lbtnExportSessionAttendance"});
                        
                    }

                    utils.endTest();

                }

                testRun.start(homeUrl, function(t){                        
                        reportbuilder.initWebTestReport(reportOptions.reportContainerSelector, t.testCount);                      
                    }, function(t){
                        reportbuilder.renderRun(t.currentRun);
                        testdataservice.endTest('StartMeetingTest');
                    }, function(t){
                        reportbuilder.renderTest(t.currentTest);
                });

            });

        });

    })

});