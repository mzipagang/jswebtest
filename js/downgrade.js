define([
  'tester',
  'testdataservice',
  'config',
  'reportbuilder'
], function(
    tester,
    testdataservice,
    config,
    reportbuilder
){

    var baseUrl = config.httpsBaseUrl;

    var accountUrl = baseUrl + "AccountManager/AnyMeetingUser.aspx?act_tab=0";
    
    var reportOptions = {reportContainerSelector:"#test"}

    var testRun;
    

    $(function(){

        testRun = tester.newRun(function(utils){

            testdataservice.startTest('DowngradeTest');

            // DOWNGRADE TESTS
            utils.newTest("DowngradeTest");

            utils.click({selector:"#DowngradeAccount_Header"});
            utils.delay(2000);

            utils.click({selector:"#ctl00_cphPageContent_TabContainer1_tpnlContactInfo_btnDowngrade"});

            utils.waitForElementToLoad({selector:"#ctl00_cphPageContent_Wizard1_dcc_0", retries:30, timeout:1000});
            utils.click({selector:"#ctl00_cphPageContent_Wizard1_dcc_0"});
            utils.delay(1000);

            utils.click({selector:"#ctl00_cphPageContent_Wizard1_StepNavigationTemplateContainerID_StepNextButton"});
        
            utils.waitForElementToLoad({selector:"#ctl00_cphPageContent_Wizard1_radioListWhyDowngrade_0", retries:30, timeout:1000});
            utils.click({selector:"#ctl00_cphPageContent_Wizard1_radioListWhyDowngrade_0"});

            utils.click({selector:"#ctl00_cphPageContent_Wizard1_StepNavigationTemplateContainerID_StepNextButton"});

            utils.waitForElementToLoad({selector:"#ctl00_cphPageContent_Wizard1_StepNavigationTemplateContainerID_StepNextButton", retries:30, timeout:1000});
            utils.click({selector:"#ctl00_cphPageContent_Wizard1_StepNavigationTemplateContainerID_StepNextButton"});
            utils.delay(30000);

            utils.waitForElementToLoad({selector:".sectionHeader", retries:30, timeout:1000});
            utils.assertTextContains({selector:".sectionHeader", value:"Thanks for using AnyMeeting"});

            utils.endTest();

        });

        testRun.start(accountUrl, function(t){                        
                reportbuilder.initWebTestReport(reportOptions.reportContainerSelector, t.testCount);                      
            }, function(t){
                reportbuilder.renderRun(t.currentRun);
                testdataservice.endTest('DowngradeTest');
            }, function(t){
                reportbuilder.renderTest(t.currentTest);
        });

    })

});