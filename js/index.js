define([
    'jquery',
    'testdataservice',
    'config'
], function(
    $,
    testdataservice,
    config
){

    $(function(){

        var ii;
        var interval;
        
        var tests = config.tests;

        // reset test state
        for(ii=0; ii<tests.length; ii++){
            testdataservice.endTest(tests[ii].testName);
        }

        $("#endRun").prop("disabled", true);
        $("#startRun").click(function(){
            ii=0;
            $("#frameholder").html(''); // clear
            $("#frameholder").append('<iframe src="" id="testWindow' + ii + '"" height="400px" width="1280px"></iframe>');
            $("#testWindow"+ii).attr("src", config.httpsBaseUrl + 'webtests/' + tests[ii].url);

            interval = setInterval(function(){
                testdataservice.getTestStatus(tests[ii].testName, function(status){
                    if(!status){
                        ii++;
                        if(ii < tests.length){
                            $("#frameholder").append('<iframe src="" id="testWindow' + ii + '"" height="400px" width="1280px"></iframe>');
                            $("#testWindow"+ii).attr("src", config.httpsBaseUrl + 'webtests/' + tests[ii].url);
                        }
                        else{
                            clearInterval(interval);
                            $("#startRun").prop("disabled", false);
                            $("#endRun").prop("disabled", true);
                        }
                    }
                });
            }, 30000);

            $(this).prop("disabled", true);
            $("#endRun").prop("disabled", false);
        });
    	
        $("#endRun").click(function(){
            clearInterval(interval);
            $("#startRun").prop("disabled", false);
            $("#endRun").prop("disabled", true);
        });
    })

});