define([
  'tester',
  'testdataservice',
  'config',
  'reportbuilder'
], function(
    tester,
    testdataservice,
    config,
    reportbuilder
){

    var baseUrl = config.httpsBaseUrl;

    var signupUrl = baseUrl + "AccountManager/AnyMeetingSignUp.aspx";
    var logoutUrl = baseUrl + "AccountManager/Logout.aspx";
    
    var reportOptions = {reportContainerSelector:"#test"}

    var testRun;
    

    $(function(){

        testRun = tester.newRun(function(utils){

            var ii=0;

            testdataservice.getTestData('signupdata.xls', function(signupdata){

                testdataservice.startTest('SignupTest');

                // SIGN-UP TESTS
                for(ii=0;ii<signupdata.size;ii++){

                    utils.newTest("SignupTest  " + ((!!signupdata.table.Description[ii]) ? ("(" + signupdata.table.Description[ii] + ")") : ""));

                    if(ii>0){
                        utils.loadResource({url:signupUrl});    
                    }

                    utils.waitForElementToLoad({selector:"#ctl00_ContentPlaceHolderBody_SignupWidget_txtFirstName_txtInput", retries:30, timeout:1000});

                    // fill first name
                    utils.fillText({selector:"#ctl00_ContentPlaceHolderBody_SignupWidget_txtFirstName_txtInput", value:signupdata.table.FirstName[ii]});
                    // fill last name
                    utils.fillText({selector:"#ctl00_ContentPlaceHolderBody_SignupWidget_txtLastName_txtInput", value:signupdata.table.LastName[ii]});
                    // fill email
                    utils.fillText({selector:"#ctl00_ContentPlaceHolderBody_SignupWidget_txtEmail_txtInput", value:(!!signupdata.table.Email[ii]) ? testdataservice.generateAlphabeticString() + signupdata.table.Email[ii] : ""});
                    // fill password
                    utils.fillText({selector:"#ctl00_ContentPlaceHolderBody_SignupWidget_txtPassword_txtInput", value:signupdata.table.Password[ii]});
                    // fill meeting address
                    utils.fillText({selector:"#ctl00_ContentPlaceHolderBody_SignupWidget_txtMeetingURL_txtInput", value:(!!signupdata.table.MeetingAddress[ii]) ? signupdata.table.MeetingAddress[ii] + testdataservice.generateAlphabeticString() : ""});
                    
                    if(!!signupdata.table.Timezone[ii]){
                        // set time zone
                        utils.setDropdown({selector:'#ctl00_ContentPlaceHolderBody_SignupWidget_ddlTimeZone_ddlTimeZone>option[value="' + signupdata.table.Timezone[ii] + '"]'});
                    }

                    if(!!signupdata.table.HowDidYouHear[ii]){
                        // set how did you hear
                        utils.setDropdown({selector:'#ctl00_ContentPlaceHolderBody_SignupWidget_ddlHowDidYouHear>option[value="' + signupdata.table.HowDidYouHear[ii] + '"]'});
                    }
                    
                    if(signupdata.table.AgreeToTOS[ii] === 'True'){
                        // set agree to TOS
                        utils.click({selector:'#ctl00_ContentPlaceHolderBody_SignupWidget_cbxAgreeService_spnCheckbox'});
                        utils.delay(1000);
                    }

                    // click sign up now button
                    utils.click({selector:"#ctl00_ContentPlaceHolderBody_SignupWidget_btnCreateUser"});

                    // asserts
                    if(!!signupdata.table.ErrorIcon[ii]){
                        utils.waitForElementToLoad({selector:signupdata.table.ErrorIcon[ii], retries:30, timeout:1000});
                        
                        if(signupdata.table.ErrorIcon[ii]==='#ctl00_ContentPlaceHolderBody_SignupWidget_cbxAgreeService_spnCheckbox'){
                            utils.assertStyleEquals({selector:signupdata.table.ErrorIcon[ii], attribute:"display", value:"block"});
                        }
                        else{
                            utils.assertStyleEquals({selector:signupdata.table.ErrorIcon[ii], attribute:"display", value:"inline"});
                        }
                    }
                    else{
                        // was able to sign up successfully
                        utils.waitForElementToLoad({selector:"#ctl00_cphPageTitle_lblPageHeader", retries:30, timeout:1000});
                        utils.assertTextEquals({selector:"#ctl00_cphPageTitle_lblPageHeader", value:"Congratulations, your account has been activated and is now ready for use!"});

                        // logout
                        utils.loadResource({url:logoutUrl});
                        utils.waitForElementToLoad({selector:".PageTitle", retries:30, timeout:1000});
                    }

                    utils.endTest();

                }

                 testRun.start(signupUrl, function(t){                        
                        reportbuilder.initWebTestReport(reportOptions.reportContainerSelector, t.testCount);                      
                    }, function(t){
                        reportbuilder.renderRun(t.currentRun);
                        testdataservice.endTest('SignupTest');
                    }, function(t){
                        reportbuilder.renderTest(t.currentTest);
                });

            });

        });

    })

});