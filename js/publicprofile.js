define([
  'tester',
  'testdataservice',
  'config',
  'reportbuilder'
], function(
    tester,
    testdataservice,
    config,
    reportbuilder
){

    var baseUrl = config.httpsBaseUrl;

    var homeUrl = baseUrl + "AccountManager/AnyMeetingUser.aspx?act_tab=1";
    
    var reportOptions = {reportContainerSelector:"#test"}

    var testRun;
    

    $(function(){

        testRun = tester.newRun(function(utils){

            var ii=0;

            testdataservice.getTestData('publicprofiledata.xls', function(publicprofiledata){

                testdataservice.startTest('PublicProfileTest');

                // PUBLIC PROFILE TESTS
                for(ii=0;ii<publicprofiledata.size;ii++){

                    utils.newTest("PublicProfileTest  " + ((!!publicprofiledata.table.Description[ii]) ? ("(" + publicprofiledata.table.Description[ii] + ")") : ""));

                    if(ii>0){
                        utils.loadResource({url:homeUrl});    
                    }

                    utils.waitForElementToLoad({selector:"#ctl00_cphPageContent_TabContainer1_tpnlMyWebinarHub_WebinarHub1_pnlhasPublicProfileHeader", retries:30, timeout:1000});
                    utils.click({selector:"#ctl00_cphPageContent_TabContainer1_tpnlMyWebinarHub_WebinarHub1_pnlhasPublicProfileHeader"});
                    utils.delay(1000);

                    if(publicprofiledata.table.Privacy[ii] === 'Public'){
                        utils.click({selector:"#ctl00_cphPageContent_TabContainer1_tpnlMyWebinarHub_WebinarHub1_rblhasPublicProfile_0"});
                    }
                    else if(publicprofiledata.table.Privacy[ii] === 'Private'){
                        utils.click({selector:"#ctl00_cphPageContent_TabContainer1_tpnlMyWebinarHub_WebinarHub1_rblhasPublicProfile_1"});
                    }
                    utils.delay(1000);
                    utils.click({selector:"#ctl00_cphPageContent_TabContainer1_tpnlMyWebinarHub_WebinarHub1_btnhasPublicProfileOK"});
                    utils.delay(5000);

                    utils.click({selector:"#ctl00_cphPageContent_TabContainer1_tpnlMyWebinarHub_WebinarHub1_pnlDisplayNameHeader"});
                    utils.delay(1000);
                    utils.fillText({selector:"#ctl00_cphPageContent_TabContainer1_tpnlMyWebinarHub_WebinarHub1_txtDisplayName", value:publicprofiledata.table.DisplayName[ii]});

                    utils.click({selector:"#ctl00_cphPageContent_TabContainer1_tpnlMyWebinarHub_WebinarHub1_pnlURLHomePageHeader"});
                    utils.delay(1000);
                    utils.fillText({selector:"#ctl00_cphPageContent_TabContainer1_tpnlMyWebinarHub_WebinarHub1_txtURLHomePage_txtURLTextBox", value:publicprofiledata.table.WebsiteUrl[ii]});
                    utils.click({selector:"#ctl00_cphPageContent_TabContainer1_tpnlMyWebinarHub_WebinarHub1_btnURLLinkedInOK"});
                    utils.delay(5000);

                    utils.click({selector:"#ctl00_cphPageContent_TabContainer1_tpnlMyWebinarHub_WebinarHub1_pnlURLLinkedInHeader"});
                    utils.delay(1000);
                    utils.fillText({selector:"#ctl00_cphPageContent_TabContainer1_tpnlMyWebinarHub_WebinarHub1_txtURLLinkedIn_txtURLTextBox", value:publicprofiledata.table.LinkedIn[ii]});
                    utils.click({selector:"#ctl00_cphPageContent_TabContainer1_tpnlMyWebinarHub_WebinarHub1_btnURLLinkedInOK"});
                    utils.delay(5000);

                    utils.click({selector:"#ctl00_cphPageContent_TabContainer1_tpnlMyWebinarHub_WebinarHub1_pnlTwitterUserNameHeader"});
                    utils.delay(1000);
                    utils.fillText({selector:"#ctl00_cphPageContent_TabContainer1_tpnlMyWebinarHub_WebinarHub1_txtTwitterUserName", value:publicprofiledata.table.Twitter[ii]});

                    utils.click({selector:"#ctl00_cphPageContent_TabContainer1_tpnlMyWebinarHub_WebinarHub1_pnlAboutHeader"});
                    utils.delay(1000);
                    utils.fillText({selector:".ajax__html_editor_extender_texteditor:lt(1)", nonInput:true, value:publicprofiledata.table.PersonalInfo[ii]});
                    utils.click({selector:"#ctl00_cphPageContent_TabContainer1_tpnlMyWebinarHub_WebinarHub1_btnAboutOK"});
                    utils.delay(5000);

                    utils.click({selector:"#ctl00_cphPageContent_TabContainer1_tpnlMyWebinarHub_WebinarHub1_pnlIndustryHeader"});
                    utils.delay(1000);
                    utils.setDropdown({selector:'#ctl00_cphPageContent_TabContainer1_tpnlMyWebinarHub_WebinarHub1_ddlIndustry>option[value="' + publicprofiledata.table.Industry[ii] + '"]'});

                    utils.click({selector:"#ctl00_cphPageContent_TabContainer1_tpnlMyWebinarHub_WebinarHub1_pnlInterestsHeader"});
                    utils.delay(1000);
                    utils.fillText({selector:".ajax__html_editor_extender_texteditor:gt(0)", nonInput:true, value:publicprofiledata.table.Interests[ii]});
                    utils.click({selector:"#ctl00_cphPageContent_TabContainer1_tpnlMyWebinarHub_WebinarHub1_btnInterestsOK"});
                    utils.delay(5000);

                    utils.click({selector:"#ctl00_cphPageContent_TabContainer1_tpnlMyWebinarHub_WebinarHub1_pnlKeywordsHeader"});
                    utils.delay(1000);
                    utils.fillText({selector:"#ctl00_cphPageContent_TabContainer1_tpnlMyWebinarHub_WebinarHub1_txtKeywords", value:publicprofiledata.table.Keywords[ii]});
                    utils.click({selector:"#ctl00_cphPageContent_TabContainer1_tpnlMyWebinarHub_WebinarHub1_btnKeywordsOK"});
                    utils.delay(5000);
                    
                    if(publicprofiledata.table.Description[ii] === 'Valid'){
                        utils.waitForElementToLoad({selector:"#ctl00_cphPageContent_TabContainer1_tpnlMyWebinarHub_WebinarHub1_imgInterests[src='images/icons/checkbox_checked.png']", retries:30, timeout:1000});
                        // look for green checks
                        utils.assertElementExists({selector:"#ctl00_cphPageContent_TabContainer1_tpnlMyWebinarHub_WebinarHub1_imghasPublicProfile[src='images/icons/checkbox_checked.png']"});
                        utils.assertElementExists({selector:"#ctl00_cphPageContent_TabContainer1_tpnlMyWebinarHub_WebinarHub1_imgDisplayName[src='images/icons/checkbox_checked.png']"});
                        utils.assertElementExists({selector:"#ctl00_cphPageContent_TabContainer1_tpnlMyWebinarHub_WebinarHub1_imgURLHomePage[src='images/icons/checkbox_checked.png']"});
                        utils.assertElementExists({selector:"#ctl00_cphPageContent_TabContainer1_tpnlMyWebinarHub_WebinarHub1_imgURLLinkedIn[src='images/icons/checkbox_checked.png']"});
                        utils.assertElementExists({selector:"#ctl00_cphPageContent_TabContainer1_tpnlMyWebinarHub_WebinarHub1_imgTwitterUserName[src='images/icons/checkbox_checked.png']"});
                        utils.assertElementExists({selector:"#ctl00_cphPageContent_TabContainer1_tpnlMyWebinarHub_WebinarHub1_imgAbout[src='images/icons/checkbox_checked.png']"});
                        utils.assertElementExists({selector:"#ctl00_cphPageContent_TabContainer1_tpnlMyWebinarHub_WebinarHub1_imgIndustry[src='images/icons/checkbox_checked.png']"});
                        utils.assertElementExists({selector:"#ctl00_cphPageContent_TabContainer1_tpnlMyWebinarHub_WebinarHub1_imgInterests[src='images/icons/checkbox_checked.png']"});
                        utils.assertElementExists({selector:"#ctl00_cphPageContent_TabContainer1_tpnlMyWebinarHub_WebinarHub1_imgKeywords[src='images/icons/checkbox_checked.png']"});    
                    }
                    else if(publicprofiledata.table.Description[ii] === 'Invalid LinkedIn'){
                        utils.waitForElementToLoad({selector:"#ctl00_cphPageContent_TabContainer1_tpnlMyWebinarHub_WebinarHub1_lblLinkedInError", retries:30, timeout:1000});
                        utils.assertTextContains({selector:"#ctl00_cphPageContent_TabContainer1_tpnlMyWebinarHub_WebinarHub1_lblLinkedInError", value:"There was an issue connecting your LinkedIn profile."});
                    }
                    else if(publicprofiledata.table.Description[ii] === 'XSS'){
                        utils.assertElementExists({selector:"script[src='xss.js']", failIfTrue:true});
                    }
                    
                    utils.endTest();

                }

                 testRun.start(homeUrl, function(t){                        
                        reportbuilder.initWebTestReport(reportOptions.reportContainerSelector, t.testCount);                      
                    }, function(t){
                        reportbuilder.renderRun(t.currentRun);
                        testdataservice.endTest('PublicProfileTest');
                    }, function(t){
                        reportbuilder.renderTest(t.currentTest);
                });

            });

        });

    })

});