define([
  'tester',
  'testdataservice',
  'config',
  'reportbuilder'
], function(
    tester,
    testdataservice,
    config,
    reportbuilder
){

    var baseUrl = config.httpsBaseUrl;

    var homeUrl = baseUrl + "AccountManager/AnyMeeting.aspx";
    
    var reportOptions = {reportContainerSelector:"#test"}

    var testRun;
    

    $(function(){

        testRun = tester.newRun(function(utils){

            var ii=0;

            testdataservice.getTestData('completemeetingsurveydata.xls', function(completemeetingsurveydata){

                testdataservice.startTest('CompleteMeetingSurveyTest');

                // COMPLETE MEETING SURVEY TESTS
                for(ii=0;ii<completemeetingsurveydata.size;ii++){

                    utils.newTest("CompleteMeetingSurveyTest  " + ((!!completemeetingsurveydata.table.Description[ii]) ? ("(" + completemeetingsurveydata.table.Description[ii] + ")") : ""));

                    
                    if(ii==0){
                        utils.waitForElementToLoad({selector:"#__tab_ctl00_cphPageContent_tabsMain_tpnlUpcoming", retries:30, timeout:1000});
                        // click upcoming meetings tab
                        utils.click({selector:"#__tab_ctl00_cphPageContent_tabsMain_tpnlUpcoming"});

                        utils.waitForElementToLoad({selector:"#ctl00_cphPageContent_tabsMain_tpnlUpcoming_gvUpcomingInvitations_ctl03_btnGvEditMeeting", retries:30, timeout:1000});
                        // click view meeting details
                        utils.runSource({source:"javascript:__doPostBack('ctl00$cphPageContent$tabsMain$tpnlUpcoming$gvUpcomingInvitations$ctl03$btnGvEditMeeting','')"});

                        utils.waitForElementToLoad({selector:"#ctl00_cphPageContent_txtSurveyURL", retries:30, timeout:1000});
                        // go to registration form
                        utils.clickAnchor({selector:"#ctl00_cphPageContent_txtSurveyURL"});
                    }
                    
                    utils.waitForElementToLoad({selector:"#Survey1_txtFirstName", retries:30, timeout:1000});
                    // fill first name
                    utils.fillText({selector:"#Survey1_txtFirstName", value:completemeetingsurveydata.table.FirstName[ii]});

                    // fill last name
                    utils.fillText({selector:"#Survey1_txtLastName", value:completemeetingsurveydata.table.LastName[ii]});

                    // fill email
                    utils.fillText({selector:"#Survey1_txtEmail", value:completemeetingsurveydata.table.Email[ii]});

                    // fill address
                    utils.fillText({selector:"#Survey1_txtAddress", value:completemeetingsurveydata.table.Address[ii]});

                    // fill city
                    utils.fillText({selector:"#Survey1_txtCity", value:completemeetingsurveydata.table.City[ii]});

                    // fill state
                    utils.fillText({selector:"#Survey1_txtState", value:completemeetingsurveydata.table.State[ii]});

                    // fill zip
                    utils.fillText({selector:"#Survey1_txtZip", value:completemeetingsurveydata.table.Zip[ii]});

                    // fill country
                    utils.fillText({selector:"#Survey1_txtCountry", value:completemeetingsurveydata.table.Country[ii]});

                    // fill phone
                    utils.fillText({selector:"#Survey1_txtPhone", value:completemeetingsurveydata.table.Phone[ii]});

                    // fill organization
                    utils.fillText({selector:"#Survey1_txtOrganization", value:completemeetingsurveydata.table.Organization[ii]});

                    // fill job title
                    utils.fillText({selector:"#Survey1_txtJobTitle", value:completemeetingsurveydata.table.JobTitle[ii]});

                    // fill industry
                    utils.fillText({selector:"#Survey1_txtIndustry", value:completemeetingsurveydata.table.Industry[ii]});

                    // fill comments
                    utils.fillText({selector:"#Survey1_txtComments", value:completemeetingsurveydata.table.Comments[ii]});

                    // set presenter rating
                    utils.click({selector:"#Survey1_rblRatingPresenter_" + completemeetingsurveydata.table.Presenter[ii]});

                    // set content rating
                    utils.click({selector:"#Survey1_rblRatingContent_" + completemeetingsurveydata.table.Content[ii]});

                    // set technology rating
                    utils.click({selector:"#Survey1_rblRatingTechnology_" + completemeetingsurveydata.table.Technology[ii]});

                    // select answer
                    utils.setDropdown({selector:"#Survey1_CustomFieldsSurvey_rptCustomFieldsInsert_ctl00_CustomField1_Survey1_CustomFieldsSurvey_rptCustomFieldsInsert_ctl00_CustomField1>option[value='" + completemeetingsurveydata.table.Answer[ii] + "']"});
 

                    // assert header and footer text
                    utils.assertTextContains({selector:"#Survey1_lblCustomTextTop", value:completemeetingsurveydata.table.HeaderText[ii]});
                    utils.assertTextContains({selector:"#Survey1_lblCustomTextBottom", value:completemeetingsurveydata.table.FooterText[ii]});

                    utils.click({selector:"#Survey1_btnSubmit"});
                    utils.delay(1000);

                    if(completemeetingsurveydata.table.Description[ii] === 'Missing First Name'){
                        utils.assertTextContains({selector:"#Survey1_rfvFirstname", value:"Please enter First Name"});
                    }
                    else if(completemeetingsurveydata.table.Description[ii] === 'Missing Last Name'){
                        utils.assertTextContains({selector:"#Survey1_rfvLastName", value:"Please enter Last Name"});
                    }
                    else if(completemeetingsurveydata.table.Description[ii] === 'Invalid Email'){
                        utils.assertTextContains({selector:"#Survey1_revEmail", value:"Please enter a valid Email"});   
                    }
                    else if(completemeetingsurveydata.table.Description[ii] === 'Missing Email'){
                        utils.assertTextContains({selector:"#Survey1_rfvEmail", value:"Please enter Email"});
                    }
                    else if(completemeetingsurveydata.table.Description[ii] === 'Missing Address'){
                        utils.assertTextContains({selector:"#Survey1_rfvAddress", value:"Please enter Address"});
                    }
                    else if(completemeetingsurveydata.table.Description[ii] === 'Missing City'){
                        utils.assertTextContains({selector:"#Survey1_rfvCity", value:"Please enter City"});
                    }
                    else if(completemeetingsurveydata.table.Description[ii] === 'Missing State'){
                        utils.assertTextContains({selector:"#Survey1_rfvState", value:"Please enter State"});
                    }
                    else if(completemeetingsurveydata.table.Description[ii] === 'Missing Zip'){
                        utils.assertTextContains({selector:"#Survey1_rfvZip", value:"Please enter Zip"});
                    }
                    else if(completemeetingsurveydata.table.Description[ii] === 'Missing Country'){
                        utils.assertTextContains({selector:"#Survey1_rfvCountry", value:"Please enter Country"});
                    }
                    else if(completemeetingsurveydata.table.Description[ii] === 'Missing Phone'){
                        utils.assertTextContains({selector:"#Survey1_rfvPhone", value:"Please enter Phone"});
                    }
                    else if(completemeetingsurveydata.table.Description[ii] === 'Missing Organization'){
                        utils.assertTextContains({selector:"#Survey1_rfvOrganization", value:"Please enter Organization"});
                    }
                    else if(completemeetingsurveydata.table.Description[ii] === 'Missing Job Title'){
                        utils.assertTextContains({selector:"#Survey1_rfvJobTitle", value:"Please enter Job Title"});
                    }
                    else if(completemeetingsurveydata.table.Description[ii] === 'Missing Industry'){
                        utils.assertTextContains({selector:"#Survey1_rfvIndustry", value:"Please enter Industry"});
                    }
                    else if(completemeetingsurveydata.table.Description[ii] === 'Missing Comments'){
                        utils.assertTextContains({selector:"#Survey1_rfvComments", value:"Please enter Comments"});
                    }
                    else if(completemeetingsurveydata.table.Description[ii] === 'Missing Answer'){
                        utils.assertTextContains({selector:"#Survey1_CustomFieldsSurvey_rptCustomFieldsInsert_ctl00_CustomField1_Survey1_CustomFieldsSurvey_rptCustomFieldsInsert_ctl00_CustomField1_rev", value:"Please select an answer"});
                    }
                    else{
                        utils.delay(5000);
                    }
                  
                    utils.endTest();

                }

                 testRun.start(homeUrl, function(t){                        
                        reportbuilder.initWebTestReport(reportOptions.reportContainerSelector, t.testCount);                      
                    }, function(t){
                        reportbuilder.renderRun(t.currentRun);
                        testdataservice.endTest('CompleteMeetingSurveyTest');
                    }, function(t){
                        reportbuilder.renderTest(t.currentTest);
                });

            });

        });

    })

});